angular.module(appTeclo).controller('ubicacionSucursalesController',
		function($scope, $filter,$timeout,ubicacionService, growl) {
 
	
	$scope.searchVO = {typeSearchSeg: [],value: undefined,viewMode:'status' };
	$scope.lists = {};
	
	$scope.viewForm = {mostrarMapa : true, colForm: 'col-md-12', colMapa : 'col-md-12', icon:'fa fa-chevron-left'};
	
	onInit = () => {
		
		ubicacionService.getTypeSearchSeg()
			.success(list=>{ $scope.lists.typeSearchSeg = list; $scope.searchVO.typeSearchSeg = list[0];})
			.error(error => { growl.error('Error al cargar los tipos de búsqueda')});
		ubicacionService.getSucursales()
			.success(list =>{$scope.lists.sucursales = list; $scope.searchVO.sucursales = list[0];})
			.error(error => { growl.error('Error al cargar las sucursales ')});
	
	};
	
	onInit();
	
	
//	$scope.neighborhoods = [ 
//	 
//	        {lat: 19.412008, lng: -99.261107}
//	      ];
	 
	
	$scope.gMap = null;
	$scope.markers = [];
 
	 $scope.initMap = function() {
 		 var cdmx = {lat: 19.431812, lng: -99.132233};
	      
	      //Instancia del mapa de google
 		 
		 $scope.gMap = new google.maps.Map(document.getElementById('map'), {
	          center: cdmx,
	          zoom: 12,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        });
	      
		
 	}

	 $scope.initMapSuc = function() {
 		 var cdmx = {lat: 19.431812, lng: -99.132233};
	      
	      //Instancia del mapa de google
 		 
		 $scope.gMap = new google.maps.Map(document.getElementById('map'), {
	          center: cdmx,
	          zoom: 11,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        });
	      
		
 	}	 
	 
	 $scope.nuevoMarker = function(content) {
		 
			var coordenadas = {
					lat: content.latitud,
					lng: content.longitud
	        };
			 
		    var marker = new google.maps.Marker({
	                position: coordenadas,
	                map: $scope.gMap,
	                draggable: false,
	               // icon : icon,
	                animation: google.maps.Animation.DROP
	         });
			 
			google.maps.event.addListener(marker, 'click', (function (marker) { return function () {
				$scope.openInfoWindow($scope.gMap, marker, content);
	            }
	        })(marker));
			return marker;
		}
		
	 
	 $scope.openInfoWindow =function(map, marker, content) {
	     var infoWindow = new google.maps.InfoWindow;
	     var markerLatLng = marker.getPosition();

	     infoWindow.setContent([
	    	 '<div style="text-align:center;">',
	    	 content.nbTipoSucursal,
	         '<br/>Dirección:'+ content.direccion,
	         '<br/>Teléfono:'+ content.numTelefono
	     ].join(''));
	      infoWindow.open(map, marker,content);
	 }
	 
	 
	$scope.addMarkerWithTimeout = function (position, timeout) {
				
        window.setTimeout(function() {
        	$scope.markers.push(new google.maps.Marker({
            position: position,
            map: $scope.gMap,
             animation: google.maps.Animation.DROP
          }));
        }, timeout);
    }
	
	
	$scope.addMarkers = function () {
		$scope.clearMarkers();
        for (var i = 0; i < $scope.lists.sucursales.length; i++) {
        	 $scope.nuevoMarker($scope.lists.sucursales[i]);
        	 
        	 //$scope.addMarkerWithTimeout($scope.neighborhoods[i], i * 200);
        }
    } 	

	$scope.addMarkersSuc = function (sucursales) {
		$scope.clearMarkers();
        $scope.nuevoMarker(sucursales);
        
    } 
	
	$scope.clearMarkers = function () {
	        for (var i = 0; i < $scope.markers.length; i++) {
	          $scope.markers[i].setMap(null);
	        }
	        $scope.markers = [];
	      }
	
	$timeout(function(){
		$scope.initMap();
    },600);

	
	
	$scope.filterSucursal = function(typeSearchSeg, sucursales, valueInput){
		let listFilter = new Array();
		if(typeSearchSeg != null){
			switch (typeSearchSeg.cdTipoBusqueda){
			case 'TD':
				    $scope.initMap();
					$scope.addMarkers();
				break;
			
			case 'SC':
					$scope.initMapSuc();
				    $scope.addMarkersSuc(sucursales);
				break;
			
			}
		}
	};
   
		
});