angular.module(appTeclo).controller('seguimientoController',function($scope,ModalService,seguimientoService,growl,$rootScope,$document){
	
	$scope.searchVO = {typeSearchSeg: [],value: undefined,viewMode:'status' };
	$scope.lists = {};
	$scope.flags = { isMosaic:true,isList:false, isDatos:true, isTabla:false, isShowDetail: false,isShowInfo: false, isShowEstatus: false};
	$scope.DG= {};
	$scope.showModal = false;
	$scope.objectModal = {};
	$scope.catGrupoInsumos = {};
	$scope.grupoPedidoSuc = {};
	$scope._viewModalDetalle = false;
	$scope.titleModalDetalle = '';
	$scope.detailVO = {};
	$scope.nbTipoSucursal= '';
	
	$scope.flagsestatus = {
			isCar: true,
			isPay: false,
			isReceit: false,
			isEncurso: false,
			isEntrega:false
		};
	
	$scope.nextpedido = {
			listNodespaySeg: [
    {idNode:1,titleNodeSeg:"RECIBIDA",iconNode:"fa-file-text-o",isActive:true,isDeploy:true},
    {idNode:2,titleNodeSeg:"EN PREPARACION",iconNode:"fa-cart-plus",isActive:false,isDeploy:false},
    {idNode:3,titleNodeSeg:"EN CURSO",iconNode:"fa-truck",isActive:false,isDeploy:false},
    {idNode:4,titleNodeSeg:"ENTREGADA",iconNode:"fa-check",isActive:false,isDeploy:false}
			]
			
		};		
	onInit = () => {
		seguimientoService.getTypeSearchSeg()
			.success(list=>{ $scope.lists.typeSearchSeg = list; $scope.searchVO.typeSearchSeg = list[0];})
			.error(error => { growl.error('Error al cargar los tipos de búsqueda')});
		seguimientoService.getSucursales()
			.success(list =>{$scope.lists.sucursales = list; $scope.searchVO.sucursales = list[0];})
			.error(error => { growl.error('Error al cargar las sucursales ')});
		seguimientoService.getStatus().success(list =>{ $scope.lists.typeStatus = list; $scope.searchVO.typeStatus = list[0];})
			.error(error =>{ growl.error('Error al cargar los tipos de estatus')});
		seguimientoService.getSucursales().success( list => {$scope.lists.stockSucursales= list; 
        console.log($scope.lists.stockSucursales);
        })
        .error( error => { growl.error('Error al cargar los sucursales: '+error) });

		seguimientoService.getPedidoPorSucursal()
			.success( list => {$scope.lists.pedidoPorSucursal= list; 
			 $scope.searchVO.pedidoPorSucursal = list[0];
						console.log($scope.lists.pedidoPorSucursal);})
			.error( error => { growl.error('Error al cargar los sucursales: '+error) });
		
		seguimientoService.getStatusActualiza().success(list =>{ $scope.lists.typeStatusActualiza = list; $scope.searchVO.typeStatusActualiza = list[0];})
		.error(error =>{ growl.error('Error al cargar los tipos de estatus')});
	}; 
	
    
	$scope.toStepPedido = node => {
		
		let progressWizard = document.getElementById('progress-wizard');
		
		if ( node.isDeploy ) {
			
			if ( node.idNode == 1 ) {
				$scope.flagsestatus.isCar = true;
	      //	$scope.flagsestatus.isPay = false;
				$scope.flagsestatus.isReceit = false;
				$scope.flagsestatus.isEnCurso = false;
				$scope.flagsestatus.isEntrega = false;
				
				progressWizard.attributes["aria-valuenow"].value = 20;
				progressWizard.style.width = "20%";
			}
			
			else if ( node.idNode == 2 ) {
				
				$scope.flagsestatus.isCar = false;
			//	$scope.flagsestatus.isPay = true;
				$scope.flagsestatus.isReceit = true;
				$scope.flagsestatus.isEnCurso = false;
				$scope.flagsestatus.isEntrega = false;
				
				progressWizard.attributes["aria-valuenow"].value = 50;
				progressWizard.style.width = "50%";
			} 
			
			else if ( node.idNode == 3 ) {
				
				$scope.flagsestatus.isCar = false;
			//	$scope.flagsestatus.isPay = false;
				$scope.flagsestatus.isReceit = false;
				$scope.flagsestatus.isEnCurso = true;
				$scope.flagsestatus.isEntrega = false;
				
				progressWizard.attributes["aria-valuenow"].value = 70;
				progressWizard.style.width = "70%";
				
			} else if ( node.idNode == 4 ) {
				
				$scope.flagsestatus.isCar = false;
			//	$scope.flagsestatus.isPay = false;
				$scope.flagsestatus.isReceit = false;
				$scope.flagsestatus.isEnCurso = false;
				$scope.flagsestatus.isEntrega = true;

				progressWizard.attributes["aria-valuenow"].value = 100;
				progressWizard.style.width = "100%";
				
			}
            
			$scope.nextpedido.listNodespaySeg.map( nd => {
				
				if ( nd.idNode === node.idNode ) {
					
					nd.isActive = true;
					
				} else {
					nd.isActive = false;
				}
			});
		}
	};

	
	
	
//	$scope.nextEstatus = () => {
//         
//		let progressWizard = document.getElementById('progress-wizard');
//
//		$scope.flagsestatus.isCar = false;
//		$scope.flagsestatus.isPay = true;
//		$scope.flagsestatus.isReceit = true;
//		$scope.flagsestatus.isEncurso = false;
//		$scope.flagsestatus.isEntrega = false;
//		
//        debugger;
//		$scope.nextpedido.listNodespaySeg[0].isActive = false;
//		$scope.nextpedido.listNodespaySeg[0].isDeploy = true;
//		
//		$scope.nextpedido.listNodespaySeg[1].isActive = true;
//		$scope.nextpedido.listNodespaySeg[1].isDeploy = true;
//		
//		progressWizard.attributes["aria-valuenow"].value = 25;
//		progressWizard.style.width = "25%";
//	};  
	
	$scope.payProducts = () => {
		let progressWizard = document.getElementById('progress-wizard');
       
		$scope.flagsestatus.isCar = false;
	//	$scope.flagsestatus.isPay = false;
		$scope.flagsestatus.isReceit = true;
		$scope.flagsestatus.isEnCurso = false;
		$scope.flagsestatus.isEntrega = false;
		
		$scope.nextpedido.listNodespaySeg[0].isActive = false;
		$scope.nextpedido.listNodespaySeg[0].isDeploy = true;
		
		$scope.nextpedido.listNodespaySeg[1].isActive = true;
		$scope.nextpedido.listNodespaySeg[1].isDeploy = true;
		
		progressWizard.attributes["aria-valuenow"].value = 50;
		progressWizard.style.width = "50%";
	};	
	
	$scope.payEncurso = () => {
		let progressWizard = document.getElementById('progress-wizard');

		$scope.flagsestatus.isCar = false;
	//	$scope.flagsestatus.isPay = false;
		$scope.flagsestatus.isReceit = false;
		$scope.flagsestatus.isEnCurso = true;
		$scope.flagsestatus.isEntrega = false;
		
		$scope.nextpedido.listNodespaySeg[1].isActive = false;
		$scope.nextpedido.listNodespaySeg[1].isDeploy = true;
		
		$scope.nextpedido.listNodespaySeg[2].isActive = true;
		$scope.nextpedido.listNodespaySeg[2].isDeploy = true;
		
		progressWizard.attributes["aria-valuenow"].value = 70;
		progressWizard.style.width = "70%";
	};		
	
	$scope.payEntrega = () => {
		let progressWizard = document.getElementById('progress-wizard');

		$scope.flagsestatus.isCar = false;
	//	$scope.flagsestatus.isPay = false;
		$scope.flagsestatus.isReceit = false;
		$scope.flagsestatus.isEnCurso = false;
		$scope.flagsestatus.isEntrega = true;
		   debugger;
		$scope.nextpedido.listNodespaySeg[2].isActive = false;
		$scope.nextpedido.listNodespaySeg[2].isDeploy = true;
		
		$scope.nextpedido.listNodespaySeg[3].isActive = true;
		$scope.nextpedido.listNodespaySeg[3].isDeploy = true;
		
		progressWizard.attributes["aria-valuenow"].value = 100;
		progressWizard.style.width = "100%";
	};	


	
   $scope.toggleViewtab = function(tab) {
		switch(tab) {
        case 'm':
            $scope.flags.isMosaic = true;
            $scope.flags.isList = false;
			break;
        case 'l':
            $scope.flags.isMosaic = false;
            $scope.flags.isList = true;
			break;
		}
    };
    
    $scope.toggleViewtabEstatus = function(tab) {
		switch(tab) {
        case 'd':
            $scope.flags.isDatos = true;
            $scope.flags.isTabla = false;
			break;
        case 't':
            $scope.flags.isDatos = false;
            $scope.flags.isTabla = true;
			break;
		}
    };    
    
    
    alamcenScroll = function() {
		$(function(){
			$('#almacenScroll').slimScroll({
				height: '100%',
		        color: '#00243c',
		        opacity: .3,
		        size: "4px",
		        alwaysVisible: false
		    });
		});
		
		$(function(){
			$('#almacenScrollAntiguos').slimScroll({
				height: '100%',
		        color: '#00243c',
		        opacity: .3,
		        size: "4px",
		        alwaysVisible: false
		    });
		});
	};
	onInit();
	
	initController = function() {
		$('#datepicker').datepicker({
			  autoclose: true,
              startDate: '+0d',
              todayHighlight: true
		});
	}
	initController();
	
	$scope.mostrarModal = function (obj){
		$scope.showModal = true;
		if(obj != null){
			angular.copy(obj, $scope.objectModal);
			$scope.titleModalDetalle = obj.nbTipoSucursal;
		}
	};

	$scope.openModalDetalle = function(grb) {
		
		$scope._viewModalDetalle  = true; 
		if(grb!=null ){
			angular.copy(grb, $scope.objectModal);
			angular.copy(grb, $scope.detailVO);
			$scope.nbGrupoModal = grb.nbTipoSucursal;
		}
		
	}
	
	$scope.clean = function(){
		$scope.detailVO={};
	}

  
	$scope.requestSupplie = function() {
		
		growl.success('',{title: 'Pedido Actualizado'});
	}
	
	//
	$scope.filterEstatus = function(typeStatusActualiza, valueInput){
	
		if(typeStatusActualiza != null){
			switch (typeStatusActualiza.cdStatus){
			
			case 'PE':
				growl.success('',{title: 'Se cambio Estatus a: Pendiente'});
				break;
			
			case 'EN':
				growl.success('',{title: 'Se cambio Estatus a: Entrega'});
				break;

			case 'PO':
				growl.success('',{title: 'Se cambio Estatus a: Programada'});	
				break;				
			}
		}
	};	
	
	
	scrollDetalleGrupos = function() {
		$(function(){
			$('#scrollDetalleGrupos').slimScroll({
				height: '100%',
		        color: '#00243c',
		        opacity: .3,
		        size: "4px",
		        alwaysVisible: false
		    });
		});
	}
	
	
	
	// 
    $scope.showModalDetail = function (data) {
        angular.copy(data, $scope.detailVO);
        $scope.flags.isShowDetail = true;
     
        
    };
    
    $scope.quitModalDetail = function () {
    	$scope.requestSupplie();
        $scope.flags.isShowDetail = false;
        angular.copy(setObject, $scope.detailVO);
       
    };	
	
	//
    $scope.showModalInfo = function (data) {
        angular.copy(data, $scope.detailVO);
        $scope.flags.isShowInfo = true;
     
    };
    
    $scope.quitModalInfo = function () {
        $scope.flags.isShowInfo = false;
        angular.copy(setObject, $scope.detailVO);
    };	

    
	//
    $scope.showModalEstatus = function (data) {
        angular.copy(data, $scope.detailVO);
        $scope.flags.isShowEstatus = true;
     
    };
    
    $scope.quitModalEstatus = function () {
    	$scope.requestPedidoFinalizado();
        $scope.flags.isShowEstatus = false;
        angular.copy(setObject, $scope.detailVO);
    };
    
	$scope.requestPedidoFinalizado = function() {
		growl.success('',{title: 'Seguimiento Pedido Finalizado'});
	}   
 
    
});