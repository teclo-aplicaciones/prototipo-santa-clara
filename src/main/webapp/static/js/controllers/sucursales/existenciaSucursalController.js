angular.module(appTeclo).controller('existenciaSucursalController',
		function($scope,ModalService,existenciaSucursalService,growl, $timeout){
	
	$scope.searchVO = {typeSearchSeg: [],value: undefined,viewMode:'status' };
	$scope.lists = {};
	$scope.flags = { isMosaic:true,isList:false, isListProductSuc:true,isMap:false, isShowDetail: false, isShowDetailProduct: false};
	$scope.DG= {};
	$scope.showModal = false;
	$scope.objectModal = {};
	$scope.catGrupoInsumos = {};
	$scope._viewModalDetalle = false;
	$scope.titleModalDetalle = '';
	$scope.detailVO = {};
	
   
	
	
	$scope.viewForm = {mostrarMapa : true, colForm: 'col-md-12', colMapa : 'col-md-12', icon:'fa fa-chevron-left'};
	$scope.gMap = null;
	$scope.markers = [];	
	$scope.neighborhoods = [ 
        {lat: 19.4202562, lng: -99.1307741}
      ];
	
	onInit = () => {
		existenciaSucursalService.getTypeSearchSeg()
			.success(list=>{ $scope.lists.typeSearchSeg = list; $scope.searchVO.typeSearchSeg = list[0];})
			.error(error => { growl.error('Error al cargar los tipos de búsqueda')});
		existenciaSucursalService.getSucursales()
			.success(list =>{$scope.lists.sucursales = list; $scope.searchVO.sucursales = list[0];})
			.error(error => { growl.error('Error al cargar las sucursales ')});
		existenciaSucursalService.getStatus().success(list =>{ $scope.lists.typeStatus = list; $scope.searchVO.typeStatus = list[0];})
			.error(error =>{ growl.error('Error al cargar los tipos de estatus')});
		existenciaSucursalService.getSucursales().success( list => {$scope.lists.stockSucursales= list; 
        console.log($scope.lists.stockSucursales);
        })
        .error( error => { growl.error('Error al cargar los sucursales: '+error) });
		
		existenciaSucursalService.getProductoExistencia()
		.success(list =>{$scope.lists.productoExistencia = list; $scope.searchVO.productoExistencia = list[0];})
		.error(error => { growl.error('Error al cargar los productos ')});
		
		
	};
   $scope.toggleViewtab = function(tab) {
		switch(tab) {
        case 'm':
            $scope.flags.isMosaic = true;
            $scope.flags.isList = false;
			break;
        case 'l':
            $scope.flags.isMosaic = false;
            $scope.flags.isList = true;
			break;
		}
    };
    
    $scope.toggleViewtabMap = function(tab) {
		switch(tab) {
        case 'p':
            $scope.flags.isListProductSuc = true;
            $scope.flags.isMap = false;
			break;
        case 'm':
            $scope.flags.isListProductSuc = false;
            $scope.flags.isMap = true;
			break;
		}
    };    
    
    alamcenScroll = function() {
		$(function(){
			$('#almacenScroll').slimScroll({
				height: '100%',
		        color: '#00243c',
		        opacity: .3,
		        size: "4px",
		        alwaysVisible: false
		    });
		});
		
		$(function(){
			$('#almacenScrollAntiguos').slimScroll({
				height: '100%',
		        color: '#00243c',
		        opacity: .3,
		        size: "4px",
		        alwaysVisible: false
		    });
		});
	};
	onInit();
	
	initController = function() {
		$('#datepicker').datepicker({
			  autoclose: true,
              startDate: '+0d',
              todayHighlight: true
		});
	}
	initController();
	
	$scope.mostrarModal = function (obj){
		$scope.showModal = true;
		if(obj != null){
			angular.copy(obj, $scope.objectModal);
			$scope.titleModalDetalle = obj.nbTipoSucursal;
		}
	};



	scrollDetalleGrupos = function() {
		$(function(){
			$('#scrollDetalleGrupos').slimScroll({
				height: '100%',
		        color: '#00243c',
		        opacity: .3,
		        size: "4px",
		        alwaysVisible: false
		    });
		});
	}
	


	$scope.openModalDetalle = function(grp) {
		
		$scope._viewModalDetalle  = true; 
		if(grp!=null ){
			angular.copy(grp, $scope.objectModal);
			angular.copy(grp, $scope.detailVO);
			$scope.nbtituloModal = grp.nbTipoSucursal;
		}
		
	}
	
	 $scope.initMap = function() {
 		 var cdmx = {lat: 19.420238, lng: -99.130932};
	      
	      //Instancia del mapa de google
		 $scope.gMap = new google.maps.Map(document.getElementById('map'), {
	          center: cdmx,
	          zoom: 18,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        });
		 $scope.addMarkers();
 	}
	 
	 
	 $scope.nuevoMarker = function(content) {
		 
			var coordenadas = {
	            lat: content.lat,
	            lng: content.lng
	        };
			 
		    var marker = new google.maps.Marker({
	                position: coordenadas,
	                map: $scope.gMap,
	                draggable: false,
	               // icon : icon,
	                animation: google.maps.Animation.DROP
	         });
			 
			google.maps.event.addListener(marker, 'click', (function (marker) { return function () {
				$scope.openInfoWindow($scope.gMap, marker, "Helados Santa Clara Miramontes, Coyoacán,CDMX");
	            }
	        })(marker));
			return marker;
		}	 
	 
	 $scope.openInfoWindow =function(map, marker, content) {
	     var infoWindow = new google.maps.InfoWindow;
	     var markerLatLng = marker.getPosition();

	     infoWindow.setContent([
	         '<div style="text-align:center;">',
	          content,
	         '<br/>Horario: lunes - viernes	08:30–21:00',
	         '<br/>Teléfono: 01 55 5280 5122'
	         
	     ].join(''));
	      infoWindow.open(map, marker);
	 }	 
	 
		$scope.addMarkers = function () {
			$scope.clearMarkers();
	        for (var i = 0; i < $scope.neighborhoods.length; i++) {
	        	 $scope.nuevoMarker($scope.neighborhoods[i]);
	        	
	        	//$scope.addMarkerWithTimeout($scope.neighborhoods[i], i * 200);
	        }
	      }	 
	 
		$scope.clearMarkers = function () {
	        for (var i = 0; i < $scope.markers.length; i++) {
	          $scope.markers[i].setMap(null);
	        }
	        $scope.markers = [];
	      }

		
	$scope.requestSupplie = function() {
		growl.success('',{title: 'Aceptar'});
	}
	
		
	
    $scope.filterSucursal = function ( typeSearchSuc, sucursal ) {
        let listFilter = new Array();
        if (typeSearchSuc != null) {
            switch (typeSearchSuc.cdTipoBusqueda) {
                case 'SC':
                    angular.copy($scope.lists.sucursales, $scope.lists.stockProduct);
                    break;
                case 'TP':
                    for (let i = 0; i < $scope.lists.stockProductTMP.length; i++) {
                        if ($scope.lists.stockProductTMP[i].idTipoProducto === typeProduct.idTipoProducto) {
                            listFilter.push($scope.lists.stockProductTMP[i]);
                        }
                    }
                    if (listFilter.length > 0) {
                        angular.copy(listFilter, $scope.lists.stockProduct);
                    } else {
                        growl.error('No se encontraron coincidencias', { ttl: 4000 });
                        $scope.lists.stockProduct = new Array();
                    }
                    break;
                    
                case 'NP':
                    $scope.lists.stockProduct = $filter('filter')($scope.lists.stockProductTMP, valueInput);
                    if ($scope.lists.stockProduct.length == 0) {
                        growl.error('No se encontraron coincidencias', { ttl: 4000 });
                        $scope.lists.stockProduct = new Array();
                    }
                    break;
            }
        } else {
            angular.copy($scope.lists.stockProductTMP, $scope.lists.stockProduct);
            //.success(list => { angular.copy(list, $scope.lists.stockProduct); angular.copy(list, $scope.lists.stockProductTMP);})
        }
    };	
    
    
	// 
    $scope.showModalDetail = function (data) {
        angular.copy(data, $scope.detailVO);
        $scope.flags.isShowDetail = true;
      //  debugger;
        $timeout(function(){
        	$scope.initMap();
        },600);
        
    };
    
    $scope.quitModalDetail = function () {
        $scope.flags.isShowDetail = false;
        angular.copy(setObject, $scope.detailVO);
        $scope.nbtituloModal = grp.nbTipoSucursal;
    };
    
    //
    $scope.showModalDetailProduct = function (data) {
        angular.copy(data, $scope.detailVO);
        $scope.flags.isShowDetailProduct = true;
        
    };
    
    $scope.quitModalDetailProduct = function () {
        $scope.flags.isShowDetailProduct = false;
        angular.copy(setObject, $scope.detailVO);
        $scope.nbtituloModal = grp.nbTipoSucursal;
    };

		 
});