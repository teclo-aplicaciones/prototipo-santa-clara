angular.module(appTeclo)
.controller('puntoVentaController', 
function($rootScope,$scope,$timeout,ventaService,storageService,growl) {

    $scope.flags = { isIcecream:false,readymain:false,isReadyProduct:false };
    $scope.models = {
        currentProduct: {},
        tasteone:"",
        tastetwo:"",
        tastethree:"",
        priceIce:parseFloat(0).toFixed(2)
    };
    $scope.lists = {
        mainStock: [],
        bases: [
            {idBase:1,cdBase:"BQS",nbBase:"BARQUILLO SENCILLO",nuPrecio:0,blImg:"static/dist/img/svg/bases/barquillo.svg"},
            {idBase:2,cdBase:"BQV",nbBase:"BARQUILLO DE VAINILLA",nuPrecio:2,blImg:"static/dist/img/svg/bases/barquillo-vainilla.svg"},
            {idBase:3,cdBase:"BQC",nbBase:"BARQUILLO DE CHOCOLATE",nuPrecio:5,blImg:"static/dist/img/svg/bases/barquillo-chocolate.svg"},
            {idBase:4,cdBase:"BQW",nbBase:"BARQUILLO DE WAFFLE",nuPrecio:5,blImg:"static/dist/img/svg/bases/barquillo-waffle.svg"},
            {idBase:5,cdBase:"BWC",nbBase:"BARQUILLO DE WAFFLE CON CHOCOLATE",nuPrecio:8,blImg:"static/dist/img/svg/bases/barquillo-wafflechocolate.svg"},
            {idBase:6,cdBase:"CNW",nbBase:"CANASTA DE WAFFLE",nuPrecio:5,blImg:"static/dist/img/svg/bases/canasta-waffle.svg"},
            {idBase:7,cdBase:"CWC",nbBase:"CANASTA DE WAFFLE CON CHOCOLATE",nuPrecio:8,blImg:"static/dist/img/svg/bases/canasta-waffle-chocolate.svg"},
            {idBase:8,cdBase:"VAS",nbBase:"VASO",nuPrecio:0,blImg:"static/dist/img/svg/bases/vaso.svg"}
        ],
        tastes: [
            { idSabor:1,nbSabor:"Vaninilla",color:"#FDD835",isActive:false },
            { idSabor:2,nbSabor:"Chocolate",color:"#6D4C41",isActive:false },
            { idSabor:3,nbSabor:"Coco",color:"#F5F5F5",isActive:false },
            { idSabor:4,nbSabor:"Fresa",color:"#FF4081",isActive:false },
            { idSabor:5,nbSabor:"Limón",color:"#B2FF59",isActive:false },
            { idSabor:6,nbSabor:"Avellana",color:"#BCAAA4",isActive:false },
            { idSabor:7,nbSabor:"Menta",color:"#00B8D4",isActive:false },
            { idSabor:8,nbSabor:"Frambuesa",color:"#C2185B",isActive:false }
        ],
        gourmetices: [
            {idHelado:1,cdHelado:"sencillo",nbHelado:"Sencillo",nuPrecio:54,isActive:false},
            {idHelado:2,cdHelado:"doble",nbHelado:"Doble",nuPrecio:85,isActive:false},
            {idHelado:3,cdHelado:"triple",nbHelado:"Triple",nuPrecio:115,isActive:false}
        ]
    };


    onInit = () => {

        // $scope.dataLoaded = false;
        $scope.flags.readymain = true;

        // $timeout( () => {
            ventaService.getStock()
                .success(list => {
                    $scope.lists.mainStock = list;
                    $scope.lists.mainStock[0].isActive = true;
                    $scope.models.currentProduct = $scope.lists.mainStock[0];
                    $timeout( () => {
                        // $scope.dataLoaded = true;
                        $scope.flags.readymain = true;

                        $timeout( () => {
                            $scope.flags.isIcecream = true;
                            $scope.flags.isReadyProduct = true;
                            $('.customain').removeClass('opacitynone');
                        }, 500);
                    });
                })
                .error(error => { growl.error('Error al cargar los tipos de productos: ' + error) });
        // }, 400);
        $(document).ready(function(){
            $(".tastemove").droppable({
                start: function(){
                    $(this).html("Muy bien, ahora suéltalo allí!");
                },
                stop: function(){
                    $(this).html("Este elemento lo puedes arrastrar...");
                }
            });
            $(".svgproduct #bola-1").droppable({
                tolerance: "fit",
                accept: ".tastemove",
                drop: function( event, ui ) {
                    $(this).css("fill","blue");
                },
                out: function( event, ui ) {
                    $(this).html("Ahora saliste...");
                }
            });
        });
    };

    $scope.viewProduct = product => {

        // if ( product.cdTipoProducto === "CAF"
        //     || product.cdTipoProducto === "BEB"
        //     || product.cdTipoProducto === "ESPG"
        //     || product.cdTipoProducto === "ESPT" ) {

        //         return;

        // } else {
            $scope.lists.mainStock.map( item => { item.isActive = false; });
            product.isActive = true;
            $scope.models.currentProduct = product;
        // }
    
    };

    $scope.changeBase = base => {
        
        if ( base === null) {

            $scope.models.tipoHelado = null;
            $scope.models.chispas = false;
            $scope.models.lunetas = false;
            $scope.models.oreos = false;
            $scope.models.priceIce = parseFloat(0).toFixed(2);
            delete $scope.lists.tasteone;
            delete $scope.lists.tastetwo;
            delete $scope.lists.tastethree;

        } else { validateIce(); }
        
        validatePrice();
    };

    $scope.viewTastes = (ice,typeIce) => {
    
        switch ( ice ) {

            case "sencillo":
                $scope.lists.tasteone = angular.copy($scope.lists.tastes);
                delete $scope.lists.tastetwo;
                delete $scope.lists.tastethree;
                validateIce();
                break;
            case "doble":
                $scope.lists.tasteone = angular.copy($scope.lists.tastes);
                $scope.lists.tastetwo = angular.copy($scope.lists.tastes);
                delete $scope.lists.tastethree;
                validateIce();
                break;
            case "triple":
                $scope.lists.tasteone = angular.copy($scope.lists.tastes);
                $scope.lists.tastetwo = angular.copy($scope.lists.tastes);
                $scope.lists.tastethree = angular.copy($scope.lists.tastes);
                validateIce();
                break;
        }

        $scope.lists.gourmetices.map(item => { item.isActive = false; });
        typeIce.isActive = true;

        validatePrice();
    
    };

    $scope.selecTaste = (taste,ball) => {
    
        switch ( ball ) {
            case 'one':
                let ballone = $('.svgproduct #bola-1');
                let lasttasteone = $scope.models.lasttasteone;

                ballone.css('fill',taste.color);
                ballone.css('stroke',"rgba(0,0,0,0.3)");
                $scope.models.tasteone = taste.nbSabor;
                $scope.lists.tasteone.map( item => { item.isActive=false; });

                if ( lasttasteone ) {
                    $scope.lists.tasteone.map( item => {
                        if( taste.idSabor === item.idSabor ) {
                            item.isActive = true;
                        }
                    });
                } else { taste.isActive = true; }

                $scope.models.lasttasteone = angular.copy(taste);
                break;
            case 'two':
                let balltwo = $('.svgproduct #bola-2');
                let lasttastetwo = $scope.models.lasttastetwo;

                balltwo.css('fill',taste.color);
                balltwo.css('stroke',"rgba(0,0,0,0.3)");
                $scope.models.tastetwo = taste.nbSabor;
                $scope.lists.tastetwo.map( item => { item.isActive=false; });

                if ( lasttastetwo ) {
                    $scope.lists.tastetwo.map( item => {
                        if( taste.idSabor === item.idSabor ) {
                            item.isActive = true;
                        }
                    });
                } else { taste.isActive = true; }
                $scope.models.lasttastetwo = angular.copy(taste);
                break;
            case 'three':
                let ballthree = $('.svgproduct #bola-3');
                let lasttastethree = $scope.models.lasttastethree;

                ballthree.css('fill',taste.color);
                ballthree.css('stroke',"rgba(0,0,0,0.3)");
                $scope.models.tastethree = taste.nbSabor;
                $scope.lists.tastethree.map( item => { item.isActive=false; });

                if ( lasttastethree ) {
                    $scope.lists.tastethree.map( item => {
                        if( taste.idSabor === item.idSabor ) {
                            item.isActive = true;
                        }
                    });
                } else { taste.isActive = true; }
                $scope.models.lasttastethree = angular.copy(taste);
                break;
        }
    
    };

    $scope.toggleTopping = (topping, flag) => {

        switch ( topping ) {
            case 'chispas':
                let chispas = $('.svgproduct #chispas');
                chispas.css("opacity",flag?"1":"0");
                break;
            case 'lunetas':
                let lunetas = $('.svgproduct #lunetas');
                lunetas.css("opacity",flag?"1":"0");
                break;
            case 'oreos':
                let oreo = $('.svgproduct #oreo');
                oreo.css("opacity",flag?"1":"0");
                break;
        }
        
        validatePrice();

    };

    $scope.addTocar = () => {

        let producTmp = { };
        let products = $rootScope.shoppingcar.listProducts;
        let id = products.length+1;

        let img = document.getElementsByClassName('svgproduct')[0].attributes[4].nodeValue;
        let nbHelado = `Helado Gourmet - ${$scope.models.base.nbBase.toLowerCase()} ${$scope.models.tipoHelado}`;

        producTmp = {
            idProducto: id,
            lbProducto: img,
            nbProducto: nbHelado,
            nuCantidad: 1,
            nuPrecio: $scope.models.priceIce,
            nuTotal: $scope.models.priceIce
        };

        products.push(producTmp);

        $rootScope.countTotal(products);

        storageService.setShoppingcar(products);

        growl.success(`Se ha agregado ${producTmp.nbProducto} al carrito`, {ttl: 5000});

    };

    validateIce = () => {
        if ( $scope.models.lasttasteone ) {
            $timeout( () => {
                $scope.selecTaste($scope.models.lasttasteone,'one');
                validateTopping();
            }, 150);
        }
        if ( $scope.models.lasttastetwo && $scope.lists.tastetwo ) {
            $timeout( () => {
                $scope.selecTaste($scope.models.lasttastetwo,'two');
                validateTopping();
            }, 150);
        }
        if ( $scope.models.lasttastethree && $scope.lists.tastethree ) {
            $timeout( () => {
                $scope.selecTaste($scope.models.lasttastethree,'three');
                validateTopping();
            }, 150);
        }
    };

    validateTopping = () => {
        
        let chispas = $scope.models.chispas;
        let lunetas = $scope.models.lunetas;
        let oreos = $scope.models.oreos;

        if ( chispas ) {
            $timeout( () => {
                $scope.toggleTopping('chispas',chispas);
            });
        }
        if ( lunetas ) {
            $timeout( () => {
                $scope.toggleTopping('lunetas',lunetas);
            });
        }
        if ( oreos ) {
            $timeout( () => {
                $scope.toggleTopping('oreos',oreos);
            });
        }

    };

    validatePrice = () => {

        if ( !$scope.models.tipoHelado ) {
            $scope.models.priceIce = parseFloat($scope.models.base.nuPrecio).toFixed(2);
        }
        else if ( $scope.models.base ) {
            let priceBase = $scope.models.base.nuPrecio;
            let priceIce = $scope.models.priceIce;
            let priceTopping = 5;
            let icecream = $scope.lists.gourmetices.filter(ice => ice.isActive)[0];
            
            if ( $scope.models.chispas || $scope.models.lunetas || $scope.models.oreos ) {
                priceIce = icecream.nuPrecio + priceBase + priceTopping;
            } else { priceIce = icecream.nuPrecio + priceBase; }

            $scope.models.priceIce = parseFloat(priceIce).toFixed(2);
        }

    };

    onInit();

});