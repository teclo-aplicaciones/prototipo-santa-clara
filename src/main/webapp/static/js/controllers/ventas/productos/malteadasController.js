angular.module(appTeclo)
.controller('malteadasController', 
function($rootScope,$scope,$timeout,storageService,growl) {

    $scope.models = {
        tastetext:"",
        priceMilkchake: parseFloat(0).toFixed(2)
    };
    $scope.lists = {
        typemilkchakes: [
            {idTipo:1,cdTipo:"TD",nbTipo:"Tradicional",isActive:false},
            {idTipo:2,cdTipo:"GM",nbTipo:"Gourmet",isActive:false}
        ],
        sizemilkchakes: [
            {idMalteada:1,cdMalteada:"MD",nbMalteada:"Mediana",isActive:false,lbImg:"static/dist/img/svg/malteada/malteada.svg",nuPrecioTD:35,nuPrecioGM:50},
            {idMalteada:2,cdMalteada:"GR",nbMalteada:"Grande",isActive:false,lbImg:"static/dist/img/svg/malteada/malteada.svg",nuPrecioTD:45,nuPrecioGM:65}
        ],
        tastesTD: [
            { idSabor:1,nbSabor:"Cholocate",colorsup:"#795548",colorinf:"#795548",isActive:false },
            { idSabor:2,nbSabor:"Vainilla",colorsup:"#FFEE58",colorinf:"#FFEE58",isActive:false },
            { idSabor:3,nbSabor:"Fresa",colorsup:"#F06292",colorinf:"#F06292",isActive:false },
            { idSabor:4,nbSabor:"Plátano",colorsup:"#FFF176",colorinf:"#FFF176",isActive:false },
            { idSabor:5,nbSabor:"Coco",colorsup:"#E0E0E0",colorinf:"#E0E0E0",isActive:false },
        ],
        tastesGM: [
            { idSabor:1,nbSabor:"Cajeta",colorsup:"#e57373",colorinf:"#e57373",isActive:false },
            { idSabor:2,nbSabor:"Gansito",colorsup:"#BDBDBD",colorinf:"#BDBDBD",isActive:false },
            { idSabor:3,nbSabor:"Caramillo de Butchers and Sons",colorsup:"#FFD740",colorinf:"#FFD740",isActive:false },
            { idSabor:4,nbSabor:"Tamachi de Helado Obscuro",colorsup:"#ff5252",colorinf:"#ff5252",isActive:false },
            { idSabor:5,nbSabor:"Huevito Kinder con Vodka",colorsup:"#FFCC80",colorinf:"#FFCC80",isActive:false },
            { idSabor:6,nbSabor:"Kahlúa con café expreso",colorsup:"#616161",colorinf:"#616161",isActive:false }
        ]
    };

    onInit = () => { };

    $scope.changeType = (type,milk) => {
    
        if ( $scope.models.sizeMilkchake === "MD" ) {
            $scope.changeSize('MD',$scope.lists.sizemilkchakes[0]);
        } else if ( $scope.models.sizeMilkchake === "GR" ) {
            $scope.changeSize('GR',$scope.lists.sizemilkchakes[1]);
        }

        $scope.lists.tastesTD.map( item => { item.isActive=false; });
        $scope.lists.tastesGM.map( item => { item.isActive=false; });
        $scope.models.tastetext = "";

        let tastesvg = $('.svgproduct #sabor');
        let foam = $('.svgproduct #espuma');

        tastesvg.css('fill',"#eaeaea");
        foam.css('fill',"#efefef");

    };

    $scope.changeSize = (size,milk) => {

        if ( $scope.models.typeMilkchake === "TD" ) {
            milk.nuPrecioTD = parseFloat(milk.nuPrecioTD).toFixed(2);
            $scope.models.priceMilkchake = milk.nuPrecioTD;
            
        } else if ( $scope.models.typeMilkchake === "GM" ) {
            milk.nuPrecioGM = parseFloat(milk.nuPrecioGM).toFixed(2);
            $scope.models.priceMilkchake = milk.nuPrecioGM;
        }

    };

    $scope.selecTasteMilkchake = (taste,tp) => {
    
        let tastesvg = $('.svgproduct #sabor');
        let foam = $('.svgproduct #espuma');
        let colorgb = hexToRgb(taste.colorsup);

        tastesvg.css('fill',taste.colorsup);
        foam.css('fill',`rgba(${colorgb},0.3)`);

        if ( tp === "TD" ) { $scope.lists.tastesTD.map( item => { item.isActive=false; }); }
        else if ( tp === "GM" ) { $scope.lists.tastesGM.map( item => { item.isActive=false; }); }

        taste.isActive = true;

        $scope.models.tastetext = taste.nbSabor;

    };

    $scope.addTocarMilkchake = () => {

        let producTmp = { };
        let products = $rootScope.shoppingcar.listProducts;
        let id = products.length+1;
        let typemilkchake = $scope.models.typeMilkchake === 'TD' ? 'Tradicional' : 'Gourmet';
        let sizemilkchake = $scope.models.sizeMilkchake === 'MD' ? 'mediana' : 'grande';
        let img = document.getElementsByClassName('svgproduct')[0].attributes[4].nodeValue;
        let nbMilkchake = `Malteada ${typemilkchake} ${sizemilkchake} sabor ${$scope.models.tastetext.toLowerCase()}`;

        producTmp = {
            idProducto: id,
            lbProducto: img,
            nbProducto: nbMilkchake,
            nuCantidad: 1,
            nuPrecio: $scope.models.priceMilkchake,
            nuTotal: $scope.models.priceMilkchake
        };

        products.push(producTmp);

        $rootScope.countTotal(products);

        storageService.setShoppingcar(products);

        growl.success(`Se ha agregado ${producTmp.nbProducto} al carrito`, {ttl: 5000});

    };

    const hexToRgb = hex => {

        let precolor = hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i,(m, r, g, b) => '#' + r + r + g + g + b + b)
        .substring(1).match(/.{2}/g)
        .map(x => parseInt(x, 16));

        return `${precolor[0]},${precolor[1]},${precolor[2]}`;
    };

    onInit();

});