angular.module(appTeclo)
.controller('cafesController', 
function($rootScope,$scope,storageService,growl) {

    const priceDefault = parseFloat(0).toFixed(2);

    $scope.models = {
        tastetext:"",
        priceCoffe: priceDefault
    };
    $scope.lists = {
        typecoffe: [
            {idTipo:1,cdTipo:"VS",nbTipo:"Vaso",isActive:false},
            {idTipo:2,cdTipo:"TZ",nbTipo:"Taza",isActive:false}
        ],
        sizecoffe: [
            {idCafe:1,cdCafe:"CH",nbCafe:"Chico",isActive:false,lbImg:"static/dist/img/svg/cafes/cafe-chico.svg"},
            {idCafe:2,cdCafe:"GR",nbCafe:"Grande",isActive:false,lbImg:"static/dist/img/svg/cafes/cafe-grande.svg"}
        ],
        sizecoffeExp: [
            {idCafe:1,cdCafe:"SN",nbCafe:"Sencillo",isActive:false,lbImg:"static/dist/img/svg/cafes/cafe-sencillo.svg"},
            {idCafe:2,cdCafe:"DB",nbCafe:"Doble",isActive:false,lbImg:"static/dist/img/svg/cafes/cafe-doble.svg"}
        ],
        tastesVS: [
            { idSabor:1,nbSabor:"Americano",colorsup:"#263238",colorinf:"#263238",isActive:false, nuPrecioCH:25,nuPrecioGR:30 },
            { idSabor:2,nbSabor:"Capuccino",colorsup:"#795548",colorinf:"#795548",isActive:false, nuPrecioCH:38,nuPrecioGR:44 },
            { idSabor:3,nbSabor:"Latte",colorsup:"#BCAAA4",colorinf:"#BCAAA4",isActive:false, nuPrecioCH:38,nuPrecioGR:44 },
            { idSabor:4,nbSabor:"Moka",colorsup:"#BF360C",colorinf:"#BF360C",isActive:false, nuPrecioCH:42,nuPrecioGR:48 },
            { idSabor:5,nbSabor:"Vienés Santa Clara",colorsup:"#FFAB91",colorinf:"#FFAB91",isActive:false, nuPrecioCH:28,nuPrecioGR:34 },
        ],
        tastesTZ: [
            { idSabor:1,cdSabor:"ES",nbSabor:"Espresso",colorsup:"#263238",colorinf:"#263238",isActive:false, nuPrecioSN:25,nuPrecioDB:32 },
            { idSabor:2,cdSabor:"EC",nbSabor:"Espresso cortado",colorsup:"#263238",colorinf:"#263238",isActive:false, nuPrecioSN:26,nuPrecioDB:34 }
        ]
    };

    onInit = () => { };

    $scope.changeType = model => {

        switch ( model ) {
            case 'TZ':
                $scope.models.sizecoffe = '';
                $scope.lists.tastesVS.map( item => { item.isActive=false; });
                break;
            case 'VS':
                $scope.models.sizecoffeExp = '';
                $scope.models.tastecoffe = '';
        }
        $scope.models.priceCoffe = priceDefault;
        $scope.models.tastetext = "";
    };

    $scope.changeSize = size => {
    
        let price = 0;

        switch ( size ) {
            case 'CH':
                for ( let taste of $scope.lists.tastesVS ) {
                    if ( taste.isActive ) { price = taste.nuPrecioCH; }
                }
                break;
            case 'GR':
                for ( let taste of $scope.lists.tastesVS ) {
                    if ( taste.isActive ) { price = taste.nuPrecioGR; }
                }
                break;
            case 'SN':
                for ( let taste of $scope.lists.tastesTZ ) {
                    if ( taste.isActive ) { price = taste.nuPrecioSN; }
                }
                break;
            case 'DB':
                for ( let taste of $scope.lists.tastesTZ ) {
                    if ( taste.isActive ) { price = taste.nuPrecioDB; }
                }
                break;
            default:
                return;
        }

        $scope.models.priceCoffe = parseFloat(price).toFixed(2);
    
    };

    $scope.selecTasteCafe = (taste,tp) => {

        let price = 0;

        switch ( tp ) {
            case 'normal':
                let saborcafe = $('.svgproduct #saborcafe');
                let sizecoffe = $scope.models.sizecoffe;

                saborcafe.css('fill',taste.colorsup);

                if ( sizecoffe === 'CH' ) { price = taste.nuPrecioCH; }
                else if ( sizecoffe === 'GR' ) { price = taste.nuPrecioGR; }

                $scope.lists.tastesVS.map( item => { item.isActive=false; });
                
                break;
            case 'espresso':
                let sizecoffeExp = $scope.models.sizecoffeExp;

                if ( sizecoffeExp === 'SN' ) { price = taste.nuPrecioSN; }
                else if ( sizecoffeExp === 'DB' ) { price = taste.nuPrecioDB; }

                $scope.lists.tastesTZ.map( item => { item.isActive=false; });
                break;
            default:
                $scope.models.priceCoffe = priceDefault;
                return;

        }

        $scope.models.tastetext = taste.nbSabor;
        taste.isActive = true;
        $scope.models.priceCoffe = parseFloat(price).toFixed(2);

    };

    $scope.addTocarCoffe = () => {

        let producTmp = { };
        let products = $rootScope.shoppingcar.listProducts;
        let id = products.length+1;
        let taste = $scope.models.tastetext;
        let type = $scope.models.typecoffe === 'VS' ? 'vaso' : 'taza';
        let size = "";
        
        if ( $scope.models.typecoffe === 'VS' ) {
            size = $scope.models.sizecoffe === 'CH' ? 'chico' : 'grande';
        }
        else if ( $scope.models.typecoffe === 'TZ' ) {
            size = $scope.models.sizecoffeExp === 'SN' ? 'sencillo' : 'doble';
        }

        let img = document.getElementsByClassName('svgproduct')[0].attributes[4].nodeValue;
        let nbCoffe = `Café en ${type} ${size} ${taste.toLowerCase()}`;

        producTmp = {
            idProducto: id,
            lbProducto: img,
            nbProducto: nbCoffe,
            nuCantidad: 1,
            nuPrecio: $scope.models.priceCoffe,
            nuTotal: $scope.models.priceCoffe
        };

        products.push(producTmp);

        $rootScope.countTotal(products);

        storageService.setShoppingcar(products);

        growl.success(`Se ha agregado ${producTmp.nbProducto} al carrito`, {ttl: 5000});

    };

    onInit();

});
