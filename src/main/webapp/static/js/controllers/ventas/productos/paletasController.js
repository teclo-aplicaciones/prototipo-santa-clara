angular.module(appTeclo)
.controller('paletasController', 
function($rootScope,$scope,growl,storageService) {

    $scope.models = {
        tastetext:"",
        pricePallete: parseFloat(0).toFixed(2)
    };
    $scope.lists = {
        typepallete: [
            {idPaleta:1,cdPaleta:"LPZ",nbPaleta:"LÁPIZ",isActive:false,lbImg:"static/dist/img/svg/paletas/paleta-lapiz.svg",nuPrecio:16},
            {idPaleta:2,cdPaleta:"CRM",nbPaleta:"CREMA",isActive:false,lbImg:"static/dist/img/svg/paletas/paleta-normal.svg",nuPrecio:25},
            {idPaleta:3,cdPaleta:"AGA",nbPaleta:"AGUA",isActive:false,lbImg:"static/dist/img/svg/paletas/paleta-normal.svg",nuPrecio:25}
        ],
        tastespencil: [
            { idSabor:1,nbSabor:"Algodón de azúcar",colorsup:"#B2EBF2",colorinf:"#B2EBF2",isActive:false },
            { idSabor:2,nbSabor:"Mango con chamoy",colorsup:"#FF9100",colorinf:"#c62828",isActive:false }
        ],
        tastescream: [
            { idSabor:1,nbSabor:"Cajeta",colorsup:"#e57373",colorinf:"#e57373",isActive:false },
            { idSabor:2,nbSabor:"Chocolate",colorsup:"#795548",colorinf:"#795548",isActive:false },
            { idSabor:3,nbSabor:"Fresa",colorsup:"#F06292",colorinf:"#FF4081",isActive:false },
            { idSabor:4,nbSabor:"Napolitana",colorsup:"#FFEE58",colorinf:"#F06292",isActive:false },
            { idSabor:5,nbSabor:"Yogurt natural",colorsup:"#FFF9C4",colorinf:"#FFF9C4",isActive:false },
            { idSabor:6,nbSabor:"Vainilla",colorsup:"#FFEE58",colorinf:"#FFEE58",isActive:false }
        ],
        tasteswater: [
            { idSabor:1,nbSabor:"Jamaica",colorsup:"#d50000",colorinf:"#d50000",isActive:false },
            { idSabor:2,nbSabor:"Limón",colorsup:"#64DD17",colorinf:"#64DD17",isActive:false },
            { idSabor:3,nbSabor:"Maracuyá",colorsup:"#EEFF41",colorinf:"#EEFF41",isActive:false },
            { idSabor:4,nbSabor:"Piña con coco",colorsup:"#EEEEEE",colorinf:"#FFEA00",isActive:false }
        ],
    };

    onInit = () => { };

    $scope.changePallete = pallete => {
    
        $scope.models.pricePallete = parseFloat(pallete.nuPrecio).toFixed(2);
    
    };

    $scope.selecTastePallete = (taste,tp) => {
    
        switch ( tp ) {
            case 'pencil':
                let tastesup = $('.svgproduct #sabor-superior');
                let tasteinf = $('.svgproduct #sabor-inferior');

                tastesup.css('fill',taste.colorsup);
                tasteinf.css('fill',taste.colorinf);

                $scope.lists.tastespencil.map( item => { item.isActive=false; });
                taste.isActive = true;
                break;
            case 'cream':
            case 'water':
                let sup = $('.svgproduct #superior');
                let inf = $('.svgproduct #inferior');

                sup.css('fill',taste.colorsup);
                inf.css('fill',taste.colorinf);

                if ( tp === "cream" ) {
                    $scope.lists.tastescream.map( item => { item.isActive=false; });
                } else {
                    $scope.lists.tasteswater.map( item => { item.isActive=false; });
                }
                
                taste.isActive = true;
                break;
        }

        $scope.models.tastetext = taste.nbSabor;
    
    };

    $scope.addTocarPallete = () => {

        let producTmp = { };
        let products = $rootScope.shoppingcar.listProducts;
        let id = products.length+1;

        let img = document.getElementsByClassName('svgproduct')[0].attributes[4].nodeValue;
        let nbPallete = `Paleta de ${$scope.models.typepallete.nbPaleta.toLowerCase()} sabor ${$scope.models.tastetext.toLowerCase()}`;

        producTmp = {
            idProducto: id,
            lbProducto: img,
            nbProducto: nbPallete,
            nuCantidad: 1,
            nuPrecio: $scope.models.pricePallete,
            nuTotal: $scope.models.pricePallete
        };

        products.push(producTmp);

        $rootScope.countTotal(products);

        storageService.setShoppingcar(products);

        growl.success(`Se ha agregado ${producTmp.nbProducto} al carrito`, {ttl: 5000});

    };

    onInit();

});