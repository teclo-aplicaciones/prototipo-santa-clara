angular.module(appTeclo)
.controller('frappesController', 
function($rootScope,$scope,$timeout,storageService,growl) {

    const priceDefault = parseFloat(0).toFixed(2);
    const priceTopping = 5;

    $scope.models = {
        tastetext:"",
        priceFrappe: priceDefault
    };
    $scope.lists = {
        sizefrappe: [
            {idFrappe:1,cdFrappe:"MD",nbFrappe:"Mediano",nuPrecio:36,isActive:false},
            {idFrappe:2,cdFrappe:"GR",nbFrappe:"Grande",nuPrecio:42,isActive:false}
        ],
        tastes: [
            { idSabor:1,nbSabor:"Café",color:"#795548",colorespuma:"#D7CCC8",isActive:false },
            { idSabor:2,nbSabor:"Mandarina",color:"#FF8F00",colorespuma:"#FFECB3",isActive:false },
            { idSabor:3,nbSabor:"Moras",color:"#C51162",colorespuma:"#F8BBD0",isActive:false },
            { idSabor:4,nbSabor:"Mango",color:"#FDD835",colorespuma:"#FFF59D",isActive:false },
            { idSabor:5,nbSabor:"Chocolate",color:"#A1887F",colorespuma:"#EFEBE9",isActive:false },
            { idSabor:6,nbSabor:"Coco",color:"#BDBDBD",colorespuma:"#E0E0E0",isActive:false }
        ]
    };

    onInit = () => { };

    $scope.changeSize = frappe => {
    
        let chispas = $scope.models.chispas;
        let lunetas = $scope.models.lunetas;
        let oreos = $scope.models.oreos;
        let price = parseInt(frappe.nuPrecio);
        let taste = {};

        if ( chispas || lunetas || oreos ) { price = price + priceTopping; }

        if ( $scope.models.tastetext ) {
            for ( let tst of $scope.lists.tastes ) {
                if ( tst.isActive ) { taste = tst; }
            }
        }

        $timeout( () => {
            $scope.selecTasteFrappe(taste);
            $scope.toggleToppingFrappe('chispas',$scope.models.chispas);
            $scope.toggleToppingFrappe('lunetas',$scope.models.lunetas);
            $scope.toggleToppingFrappe('oreos',$scope.models.oreos);
        }, 150);

        $scope.models.priceFrappe = parseFloat(price).toFixed(2);

    };

    $scope.selecTasteFrappe = taste => {
    
        let envase = $('.svgproduct #envase');
        let espuma = $('.svgproduct #espuma');
        // let contornotapa = $('.svgproduct #contorno-tapa');

        envase.css('fill',taste.color);
        envase.css('stroke',"rgba(0,0,0,0.3)");
        espuma.css('fill',taste.colorespuma);

        $scope.models.tastetext = taste.nbSabor;
        $scope.lists.tastes.map( item => { item.isActive=false; });

        taste.isActive = true;
    
    };

    $scope.toggleToppingFrappe = (topping, flag) => {

        let chispas = $scope.models.chispas;
        let lunetas = $scope.models.lunetas;
        let oreos = $scope.models.oreos;
        let price = 0;

        switch ( topping ) {
            case 'chispas':
                let chispasvg = $('.svgproduct #chispas');
                chispasvg.css("opacity",flag?"1":"0");
                break;
            case 'lunetas':
                let lunetasvg = $('.svgproduct #lunetas');
                lunetasvg.css("opacity",flag?"1":"0");
                break;
            case 'oreos':
                let oreosvg = $('.svgproduct #oreos');
                oreosvg.css("opacity",flag?"1":"0");
                break;
        }

        if ( $scope.models.sizefrappe === 'MD' ) { price = $scope.lists.sizefrappe[0].nuPrecio; }
        else { price = $scope.lists.sizefrappe[1].nuPrecio; }

        price = parseInt(price);

        if ( chispas || lunetas || oreos ) { price = price + priceTopping; }

        $scope.models.priceFrappe = parseFloat(price).toFixed(2);

    };

    $scope.addTocarFrappe = () => {

        let producTmp = { };
        let products = $rootScope.shoppingcar.listProducts;
        let id = products.length+1;
        let taste = $scope.models.tastetext;
        let size = $scope.models.sizefrappe === 'MD' ? 'mediano' : 'grande';

        let img = document.getElementsByClassName('svgproduct')[0].attributes[4].nodeValue;
        let nbFrappe = `Frappe ${size} sabor ${taste.toLowerCase()}`;

        producTmp = {
            idProducto: id,
            lbProducto: img,
            nbProducto: nbFrappe,
            nuCantidad: 1,
            nuPrecio: $scope.models.priceFrappe,
            nuTotal: $scope.models.priceFrappe
        };

        products.push(producTmp);

        $rootScope.countTotal(products);

        storageService.setShoppingcar(products);

        growl.success(`Se ha agregado ${producTmp.nbProducto} al carrito`, {ttl: 5000});

    };

    onInit();

});