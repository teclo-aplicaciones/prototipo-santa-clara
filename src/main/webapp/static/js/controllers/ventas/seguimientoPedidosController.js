angular.module(appTeclo)
.controller('seguimientoPedidosController', 
function($scope,ventaService,growl) {

    $scope.lists = {
        orders: [],
        ordersReady: [],
        ordersPending: [],
        ordersScheduled: [],
        tabs: [
            {idTab:1,cdTab:"PD",nbTab:"PEDIDOS DEL DÍA",isActive:true},
            {idTab:2,cdTab:"PP",nbTab:"PEDIDOS PROGRAMADOS",isActive:false},
            {idTab:3,cdTab:"HP",nbTab:"HISTORIAL DE PEDIDOS",isActive:false}
        ]
    };

    $scope.ordersDetail = new Object();

    $scope.flags = {
        isOrderDay: true,
        isOrderScheduled: false,
        isOrderHistorial: false,
        isModalDetail: false
    };

    onInit = () => {
        
        ventaService.getOrders()
            .success( list => { 
                $scope.lists.orders = list;

                for ( let order of list ) {

                    if ( order.stPedido === "Preparado" ) {
                        order.fhEntrega = moment(new Date()).format('DD/MM/YYYY HH:mm');
                        $scope.lists.ordersReady.push(order);
                    } else if ( order.stPedido === "Pendiente" && order.fhEntrega === "" ) {
                        order.fhEntrega = moment(new Date()).format('DD/MM/YYYY HH:mm');
                        $scope.lists.ordersPending.push(order);
                    } else if ( order.stPedido === "Pendiente" && order.fhEntrega !== "" ) {
                        $scope.lists.ordersScheduled.push(order);
                    }

                }

            })
            .error( error => { growl.error('Error al cargar los pedidos') });
        
    };

    $scope.toggleTab = tab => {
    
        switch ( tab.cdTab ) {
            case "PD":
                $scope.flags.isOrderDay = true;
                $scope.flags.isOrderScheduled = false;
                $scope.flags.isOrderHistorial = false;
                break;
            case "PP":
                $scope.flags.isOrderDay = false;
                $scope.flags.isOrderScheduled = true;
                $scope.flags.isOrderHistorial = false;
                break;
            case "HP":
                $scope.flags.isOrderDay = false;
                $scope.flags.isOrderScheduled = false;
                $scope.flags.isOrderHistorial = true;
                break;
        }

        $scope.lists.tabs.map( tb => { tb.isActive = false; });
        tab.isActive = true;
    };

    $scope.showDetail = (data) => {
        angular.copy(data, $scope.ordersDetail); 
        $scope.flags.isModalDetail = true;
        // debugger;
    };

    $scope.quitDetail = () => {
        $scope.flags.isModalDetail = false;
        $scope.ordersDetail = new Object();
    };

    onInit();

});