angular.module(appTeclo)
.controller('cartaController',
function($scope,$rootScope,$timeout,$document,
    inventarioService,ventaService,storageService,growl) {

    $scope.searchVO = { typeSearch: [],value: undefined,viewMode:'status' };
    $scope.lists = { };
    $scope.flags = { isImgPromo:false,isProductdetail:false };
    $scope.cartaVO = { productDetail: {} };

    onInit = () => {

        inventarioService.getTypeSearch()
            .success( list => { $scope.lists.typeSearch = list; $scope.searchVO.typeSearch = list[0]; })
            .error( error => { growl.error('Error al cargar los tipos de búsqueda') });

        inventarioService.getTypeProduct()
            .success( list => { $scope.lists.typeProduct = list; $scope.searchVO.typeProduct = list[0]; })
            .error( error => { growl.error('Error al cargar los tipos de productos') });
        
        inventarioService.getStock()
            .success( list => { $scope.lists.stock = list; })
            .error( error => { growl.error('Error al cargar los productos') });
        
        ventaService.getPromos()
            .success( list => { $scope.lists.promos = list; })
            .error( error => { growl.error('Error al cargar las promociones') });

        $timeout( () => {
            $('#scrollProducts').slimScroll({
                height: '100%',
                color: '#00243c',
                opacity: .3,
                size: "4px",
                alwaysVisible: false
            });
        }, 100);

    };

    $scope.validateFiltercode = string => {

        let quantityProducts = document.getElementById('quantityProducts');
    
        $scope.productFind = $scope.lists.stock.find( product => {
            if ( product.flProducto === string ) {
                return product;
            }
        });
        
        if ( $scope.productFind !== undefined ) {
            $scope.array.isCode = true;
            $timeout( () => {
                quantityProducts.focus();
            }, 300);
        }
    
    };

    $scope.validateFilterquantity = string => {
    
        if ( string == "" ) {
            $scope.array.isCode = false;
        }
    
    };

    $scope.viewImgPromo = img => {
        $scope.imgPromo = img;
        $scope.flags.isImgPromo = true;
    };

    $scope.showDetailproduct = product => {
    
        $scope.cartaVO.productDetail = product;
        $scope.cartaVO.productDetail.listimages = product.listimages;
        $scope.cartaVO.productDetail.sabores = [
            { idSabor:1,nbSabor:"Vaninilla",color:"yellow",isActive:false },
            { idSabor:2,nbSabor:"Chocolate",color:"brown",isActive:false },
            { idSabor:3,nbSabor:"Mora azul",color:"steelblue",isActive:true },
            { idSabor:4,nbSabor:"Fresa",color:"pink",isActive:false }
        ];
        $scope.flags.isProductdetail = true;
    
    };

    $scope.addTocar = (product, type) => {

        let isExist = false;
        let producTmp = { };
        let products = $rootScope.shoppingcar.listProducts;
        let id = products.length+1;

        if (type === 'promo') {
            producTmp = {
                idProducto: id,
                lbProducto: product.lbPromocion,
                nbProducto: product.nbPromocion,
                nuPrecio: product.nuPrecio
            };
        } else {
            producTmp = {
                idProducto: id,
                lbProducto: product.lbImg,
                nbProducto: product.nbTipoProducto,
                nuPrecio: product.nuPrecio
            };
        }

        if (products.length !== 0) {
            products.map( pro => {
                if (pro.nbProducto === producTmp.nbProducto) {
                    isExist = true;
                    pro.nuCantidad = pro.nuCantidad+1;
                    // pro.nuPrecio = parseFloat(pro.nuPrecio);
                    pro.nuTotal = parseFloat(pro.nuPrecio) + parseFloat(producTmp.nuPrecio);
                    pro.nuPrecio = parseFloat(pro.nuPrecio).toFixed(2);
                    pro.nuTotal = parseFloat(pro.nuTotal).toFixed(2);
                }
            });

            if ( !isExist ) {
                producTmp.nuCantidad = 1;
                // producTmp.nuPrecio = parseFloat(producTmp.nuPrecio).toFixed(2);
                producTmp.nuTotal = parseFloat(producTmp.nuPrecio).toFixed(2);
                products.push(producTmp);
            }

        }  else {
            producTmp.nuCantidad = 1;
            producTmp.nuTotal = parseFloat(producTmp.nuPrecio).toFixed(2);
            products.push(producTmp);
        }

        $rootScope.countTotal(products);

        storageService.setShoppingcar(products);

        growl.success(`Se ha agregado ${producTmp.nbProducto} al carrito`, {ttl: 3000});
    };

    detectKey  = (e) => {

        if( e.altKey ) {
            switch ( e.which || e.keyCode ) {
                case 70: //F -> Focus en filtro
                    let filterProducts = document.getElementById('filterProducts');
                    filterProducts.focus();
                    e.preventDefault();
                    break;
                case 67: //C Limpiar filtro
                    $rootScope.$evalAsync( () => {
                        $scope.array.filter = "";
                    });
                    e.preventDefault();
                    break;
                case 83: //S -> Ver carrito
                    $rootScope.$evalAsync( () => {
                        $rootScope.shoppingcar.isShoppingview = !$rootScope.shoppingcar.isShoppingview;
                    });
                    e.preventDefault();
                    break;
                case 80: //P Realizar pago
                    if ( $rootScope.shoppingcar.listProducts.length !== 0 ) {
                        $rootScope.$evalAsync( () => {
                            $rootScope.shoppingcar.isShoppingview = false;
                            $rootScope.shoppingcar.isShowPay = true;
                            $timeout( () => {
                                let progressWizard = document.getElementById('progress-wizard');
                                
                                if ( $scope.flagspay.isCar ) {
                                    progressWizard.attributes["aria-valuenow"].value = 20;
                                    progressWizard.style.width = "20%";
                                } else if ( $scope.flagspay.isPay ) {
                                    progressWizard.attributes["aria-valuenow"].value = 50;
                                    progressWizard.style.width = "50%";
                                } else if ( $scope.flagspay.isReceit ) {
                                    progressWizard.attributes["aria-valuenow"].value = 100;
                                    progressWizard.style.width = "100%";
                                }
                            }, 300);
                        });
                    } else {
                        growl.warning('No hay productos en el carrito todavía', {ttl:3000});
                    }
                    e.preventDefault();
                    break;
                case 13: //Enter: 
                    if ( $scope.array.isCode ) {
                        $scope.addTocar($scope.productFind,'pro');
                    }
                    break;
            }
        }
    
    };

    $document.on("keydown", detectKey);

    onInit();

});

