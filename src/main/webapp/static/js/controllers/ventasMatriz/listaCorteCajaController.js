angular.module(appTeclo).controller('listaCorteCajaController',
		function($scope,ModalService,listaCorteCajaService,growl,$rootScope,$document, $filter){
	
    $scope.lists = {datosuser: new Array(),
    		        datosuserTMP: new Array(), 
    		        detalleticket: new Array(),
    		        detalleticketTMP: new Array(),
    		        tipoBusqueda:[{idTipoBusqueda: 1, nbTipoBusqueda:"TODO", cdTipo: "TD"},
                                  {idTipoBusqueda: 2, nbTipoBusqueda:"USUARIO", cdTipo: "US"}]};
    $scope.searchVO = {tipoBusqueda: [] ,value: undefined, viewMode:'status'};	
    
    $scope.base = {
    		priceBaseTicket:parseFloat(0).toFixed(2)
    };
   
    $scope.flags = {isShowVentasDia: false};
    
    onInit = () => {
    
    listaCorteCajaService.getDatosUser()
    .success(list => { angular.copy(list, $scope.lists.datosuser); angular.copy(list, $scope.lists.datosuserTMP); })
    .error(error => { growl.error('Error al cargar los productos: ' + error) }); 
    
    listaCorteCajaService.getDatosTicket()
    .success(lists =>{$scope.lists.datosticket = lists; angular.copy($scope.searchVO.datosticketTMP = lists[0]);})
	.error(error => { growl.error('Error al cargar los datos de usuario ')});	
    
    listaCorteCajaService.getDetalleTicket()
    .success(list =>{ angular.copy(list, $scope.lists.detalleticket); angular.copy(list, $scope.lists.detalleticketTMP); })
    .error(error =>{growl.error('Error al cargar detalleTicket: ' + error) });
    
    
    listaCorteCajaService.getHistorialCorteCaja()
    .success(lists =>{$scope.lists.historialCorteCaja = lists; angular.copy($scope.searchVO.historialCorteCajaTMP = lists[0]);})
	.error(error => { growl.error('Error al cargar los datos de usuario ')});	 
    
    };
    onInit();
    
    $scope.showModalVentasDia = function(data){
    	angular.copy(data,$scope.detailVO);
    	$scope.flags.isShowVentasDia = true;
    };
    
    $scope.showCerrarToastVentasDia = function(data){
    	angular.copy(data,$scope.detailVO);
    	$scope.flags.isShowVentasDia = true;
    };
    
 
    $scope.changeTicket = function(data){
    	
    	let listFilter = new Array();
    	let priceBaseTicket = 0;
    	angular.copy(data,$scope.detailVO);
    	
    	 for (let i = 0; i < $scope.lists.detalleticketTMP.length; i++) {
             if ($scope.lists.detalleticketTMP[i].nbDetTicket === data.nbTicket) {
            	 priceBaseTicket = parseFloat($scope.lists.detalleticketTMP[i].detimporte) + parseFloat(priceBaseTicket);
            	 $scope.base.priceBaseTicket = parseFloat(priceBaseTicket.toFixed(2));
                 listFilter.push($scope.lists.detalleticketTMP[i]);
             }
         }
    	 if(listFilter.length >0){
				angular.copy(listFilter, $scope.lists.detalleticket);
		 }else{
				growl.error('No se encontraron coincidencias',{ttl: 4000});
				$scope.lists.detalleticket = new Array();
		 }    	
    };
    
});