angular.module(appTeclo).controller('movimientoCajaController',
		function($scope,ModalService,movimientoCajaService,growl,$rootScope,$document){
	
	 $scope.lists = {
			 datosuser: new Array(),
			  datosuserTMP: new Array(),
			  allEmpleados: new Array(),
			  allEmpleadosTMP: new Array(),
			  detalleticket: new Array(),
			  detalleticketTMP: new Array(),
			 tabs: [
         {idTab:1,cdTab:"SUC",nbTab:"SUCURSALES",isActive:true},
         {idTab:2,cdTab:"PP",nbTab:"SUCURSALES",isActive:false},
         {idTab:3,cdTab:"HP",nbTab:"SUCURSALES",isActive:false}
     ]};
	 
	 $scope.flags = {
			 isSearchSucursal: true
	 };
	 
	
     $scope.searchVO = {typeSearch: [] ,value: undefined, viewMode:'status'};	
     
     onInit = () => {
    	 movimientoCajaService.getTypeSearch()
		.success(lists=>{ $scope.lists.typeSearch = lists; $scope.searchVO.typeSearch = lists[0];})
		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});  
    	 
    	 movimientoCajaService.getTypeMovimiento()
 		.success(lists=>{ $scope.lists.typeSearchMov = lists; $scope.searchVO.typeSearchMov = lists[0];})
 		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});    	
    	 
    	 movimientoCajaService.getDatosMovimiento()
  		.success(lists=>{ $scope.lists.datoMovimiento = lists; $scope.searchVO.datoMovimiento = lists[0];})
  		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});   
    	 
//    	 movimientoCajaService.getDatosUser()
// 		.success(lists =>{$scope.lists.datosuser = lists; $scope.searchVO.datosuser = lists[0];})
// 		.error(error => { growl.error('Error al cargar los datos de usuario ')});	 
    	 
    	 movimientoCajaService.getDetalleTicket()
    	    .success(list =>{ angular.copy(list, $scope.lists.detalleticket); angular.copy(list, $scope.lists.detalleticketTMP); })
    	    .error(error =>{growl.error('Error al cargar detalleTicket: ' + error) });
    	 
    	 movimientoCajaService.getDatosUser()
    	    .success(list =>{ angular.copy(list, $scope.lists.datosuser); angular.copy(list, $scope.lists.datosuserTMP); })
    	    .error(error =>{growl.error('Error al cargar detalleTicket: ' + error) });
         
    	 movimientoCajaService.getAllEmpleados()
 	    .success(list =>{ angular.copy(list, $scope.lists.allEmpleados); angular.copy(list, $scope.lists.allEmpleadosTMP); })
	    .error(error =>{growl.error('Error al cargar detalleTicket: ' + error) });
     };
     onInit();
     
     $scope.toggleTab = tab =>{
    	 switch ( tab.cdTab ){
    	 	case "SUC":
    	 		$scope.flags.isSearchSucursal = true;
    	 		break;
    	 }
    	 
    	 $scope.lists.tabs.map( tb => { tb.isActive = false; });
         tab.isActive = true;
     };
     
     $scope.changeDatos = function(data){
     
     	let listFilter = new Array();
     
     	angular.copy(data,$scope.detailVO);
     	
     	 for (let i = 0; i < $scope.lists.allEmpleadosTMP.length; i++) {
     		 
              if ($scope.lists.allEmpleadosTMP[i].cdTipoSucursal === data.cdTipoSucursal) {
             	
                  listFilter.push($scope.lists.allEmpleadosTMP[i]);
              }
          }
     	 if(listFilter.length >0){
 				angular.copy(listFilter, $scope.lists.allEmpleados);
 		 }else{
 				growl.error('No se encontraron coincidencias',{ttl: 4000});
 				$scope.lists.allEmpleados = new Array();
 		 }    	
     };     
 
     
     $scope.changeTicket = function(data){
     	debugger;
     	let listFilter = new Array();
     	let priceBaseTicket = 0;
     	angular.copy(data,$scope.detailVO);
     	
     	 for (let i = 0; i < $scope.lists.detalleticketTMP.length; i++) {
     		 debugger;
              if ($scope.lists.detalleticketTMP[i].nbDetTicket === data.foliouser) {
            	 
             	 debugger;
                  listFilter.push($scope.lists.detalleticketTMP[i]);
              }
          }
     	 if(listFilter.length >0){
     		 debugger;
 				angular.copy(listFilter, $scope.lists.detalleticket);
 		 }else{
 				growl.error('No se encontraron coincidencias',{ttl: 4000});
 				$scope.lists.detalleticket = new Array();
 		 }    	
     };
     
});
	