angular.module(appTeclo).controller('corteCajaController',
		function($scope,ModalService,corteCajaService,growl,$rootScope,$document, $filter){
	
	 $scope.lists = {datosuser: new Array(),datosuserTMP: new Array(), tipoCorte:[{idTipoCorte: 1, nbCorte:"por Turno", lbimagen: "static/dist/img/clock.png"},
		                                                                          {idTipoCorte: 2, nbCorte:"por Día", lbimagen:"static/dist/img/clock.png" }]};
     $scope.searchVO = {typeSearch: [] ,value: undefined, viewMode:'status'};	
     $scope.flags = { isMosaic:true,isList:false, isShowCerrarCaja: false, isPrimero:true,isSegundo:false, isTipoCorte: true, isfinalizarCorte: false, isShowInfoMovimientoDia: false};
     
     onInit = () => {
    	 corteCajaService.getTypeSearch()
		.success(lists=>{ $scope.lists.typeSearch = lists; $scope.searchVO.typeSearch = lists[0];})
		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});  
    	 
    	 corteCajaService.getTypeMovimiento()
 		.success(lists=>{ $scope.lists.typeSearchMov = lists; $scope.searchVO.typeSearchMov = lists[0];})
 		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});    	
    	 
//    	 corteCajaService.getDatosUser()
//		.success(lists =>{$scope.lists.datosuser = lists; $scope.searchVO.datosuser = lists[0];})
//		.error(error => { growl.error('Error al cargar los datos de usuario ')});	
    	 
    	 corteCajaService.getDatosUser()
        .success(list => { angular.copy(list, $scope.lists.datosuser); angular.copy(list, $scope.lists.datosuserTMP); })
        .error(error => { growl.error('Error al cargar los productos: ' + error) }); 
    	 
    	 
    	 corteCajaService.getEstatusUser()
		.success(lists =>{$scope.lists.estatususer = lists; $scope.searchVO.estatususer = lists[0];})
		.error(error => { growl.error('Error al cargar los datos de usuario ')});	
    	 
    	 corteCajaService.getDatosMovimiento()
  		.success(lists=>{ $scope.lists.datoMovimiento = lists; $scope.searchVO.datoMovimiento = lists[0];})
  		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});      	 

    	 corteCajaService.getAperturaInicial()
		.success(lists =>{$scope.lists.aperturainicial = lists; $scope.searchVO.aperturainicial = lists[0];})
		.error(error => { growl.error('Error al cargar los datos de apertura inicial ')});	    	 
    	 
     };
     onInit();
	   $scope.toggleViewtab = function(tab) {
			switch(tab) {
	        case 'm':
	            $scope.flags.isMosaic = true;
	            $scope.flags.isList = false;
				break;
	        case 'l':
	            $scope.flags.isMosaic = false;
	            $scope.flags.isList = true;
				break;
			}
	    };
	
	    
		$scope.filterEstatus = function(typeSearch,estatususer,valueInput){
			debugger;
			let listsFilter = new Array();
			if (typeSearch !=null){
				debugger;
				switch(typeSearch.cdTipoBusqueda){
					
					case'TD':
							angular.copy($scope.lists.datosuserTMP,$scope.lists.datosuser);
						break;
						
					case'ST':
						debugger;
						for(let i=0; i<$scope.lists.datosuserTMP.length; i++){
							if($scope.lists.datosuserTMP[i].nbStatuscorte === estatususer.nbStatus){
								listsFilter.push($scope.lists.datosuserTMP[i]);
							}
						}
						if(listsFilter.length >0){
							angular.copy(listsFilter, $scope.lists.datosuser);
						}else{
							growl.error('No se encontraron coincidencias',{ttl: 4000});
							$scope.lists.datosuser = new Array();
						}
						break;
						
					case 'FL':
						
						$scope.lists.datosuser = $filter('filter')($scope.lists.datosuserTMP, valueInput);
	                    if ($scope.lists.datosuser.length == 0) {
	                        growl.error('No se encontraron coincidencias', { ttl: 4000 });
	                        $scope.lists.datosuser = new Array();
	                    }
						break;
				}
			}
		};	    
   
	    
	    $scope.showModalCerrarCaja = function (data) {
	        angular.copy(data, $scope.detailVO);
	        $scope.flags.isShowCerrarCaja = true;
	        $scope.flags.isTipoCorte = true;
	        $scope.flags.isCorteCaja = false;	
	        $scope.flags.isfinalizarCorte =false;
	    };		    
	    
	    $scope.quitModalCerrarCaja = function () {
	    	growl.error('Se Cancelo Corte de Caja', { ttl: 4000 });
	    	$scope.flags.isShowCerrarCaja = false;
	    	$scope.flags.isTipoCorte = false;
	    	$scope.flags.isCorteCaja = false;
	    	$scope.flags.isfinalizarCorte =false;
	        angular.copy(setObject, $scope.detailVO);
	    };	
	    
	    $scope.mostrarSegundo=function (){
	    	$scope.flags.isTipoCorte = false;
	    	$scope.flags.isCorteCaja =true;
	    	$scope.flags.isfinalizarCorte =false;
	    	angular.copy($scope.detailVO);
	    };
	    $scope.mostrarTercero=function (){
	    	$scope.flags.isTipoCorte = false;
	    	$scope.flags.isCorteCaja =false;
	    	$scope.flags.isfinalizarCorte =true;
	    	angular.copy($scope.detailVO);
	    };
	    
	    $scope.showModalInfoMovParcial =function (data){
	    	angular.copy(data, $scope.detailVO);
	    	$scope.flags.isShowInfoMovimientoDia = true;
	    	
	    	
	    };
	    
	    
		$scope.printTicket = receit => {
			
			let ticket = document.getElementById(receit);
			let tickimp= window.open(' ','poimpr');
			
			tickimp.document.write(ticket.innerHTML);
			tickimp.document.close();
			tickimp.print();
			tickimp.close();
		
            $scope.quitModalAgignarApertura();
                	
		};
	    
});