angular.module(appTeclo).controller('aperturaCajaController',
		function($scope, ModalService, aperturaCajaService, growl, showAlert,$filter){
   
    $scope.lists = {datosuser: new Array(),datosuserTMP: new Array()};
	$scope.searchVO = {typeSearch: [] ,value: undefined, viewMode:'status'};	
	$scope.flags = { isMosaic:true,isList:false, isShowAsigApertura: false,isShowDatosPersonales:false ,isPrimero:true,isSegundo:false,isTercero:false,isCuarto:false};
	$scope.detailVO = {};
	onInit = () => {
		
		aperturaCajaService.getTypeSearch()
		.success(lists=>{ $scope.lists.typeSearch = lists; $scope.searchVO.typeSearch = lists[0];})
		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});
		
//		aperturaCajaService.getDatosUser()
//		.success(lists =>{$scope.lists.datosuser = lists; $scope.searchVO.datosuser = lists[0];})
//		.error(error => { growl.error('Error al cargar los datos de usuario ')});	
         //
		aperturaCajaService.getDatosUser()
        .success(list => { angular.copy(list, $scope.lists.datosuser); angular.copy(list, $scope.lists.datosuserTMP); })
        .error(error => { growl.error('Error al cargar los productos: ' + error) });
		//
		aperturaCajaService.getEstatusUser()
		.success(lists =>{$scope.lists.estatususer = lists; angular.copy($scope.searchVO.estatususerTMP = lists[0]);})
		.error(error => { growl.error('Error al cargar los datos de usuario ')});			
		
		aperturaCajaService.getAperturaInicial()
		.success(lists =>{$scope.lists.aperturainicial = lists; $scope.searchVO.aperturainicial = lists[0];})
		.error(error => { growl.error('Error al cargar los datos de apertura inicial ')});		
		
	};  
	
	onInit();
	
	$scope.filterEstatus = function(typeSearch,estatususer,valueInput){
		
		let listsFilter = new Array();
		if (typeSearch !=null){
			switch(typeSearch.cdTipoBusqueda){
				
				case'TD':
						angular.copy($scope.lists.datosuserTMP,$scope.lists.datosuser);
					break;
					
				case'ST':
					//
					for(let i=0; i<$scope.lists.datosuserTMP.length; i++){
						if($scope.lists.datosuserTMP[i].nbStatusCaja === estatususer.nbStatus){
							listsFilter.push($scope.lists.datosuserTMP[i]);
						}
					}
					if(listsFilter.length >0){
						angular.copy(listsFilter, $scope.lists.datosuser);
					}else{
						growl.error('No se encontraron coincidencias',{ttl: 4000});
						$scope.lists.datosuser = new Array();
					}
					break;
					
				case 'FL':
					
					$scope.lists.datosuser = $filter('filter')($scope.lists.datosuserTMP, valueInput);
                    if ($scope.lists.datosuser.length == 0) {
                        growl.error('No se encontraron coincidencias', { ttl: 4000 });
                        $scope.lists.datosuser = new Array();
                    }
					break;
			}
		}
	};


	   $scope.toggleViewtab = function(tab) {
			switch(tab) {
	        case 'm':
	            $scope.flags.isMosaic = true;
	            $scope.flags.isList = false;
				break;
	        case 'l':
	            $scope.flags.isMosaic = false;
	            $scope.flags.isList = true;
				break;
			}
	    };
	    


		  $scope.retrocederDos = () => {
			  $scope.flags.isPrimero = true;
			  $scope.flags.isSegundo = false;
			  $scope.flags.isTercero = false;
			  $scope.flags.isCuarto = false;
		  };  	    
	    
	  
	  $scope.retrocederUno = () => {
		  $scope.flags.isPrimero = false;
		  $scope.flags.isSegundo = true;
		  $scope.flags.isTercero = false;
		  $scope.flags.isCuarto = false;
	  };  

	  
	  
	  $scope.pasoUno = () => {
		  $scope.flags.isPrimero = true;
		  $scope.flags.isSegundo = false;
		  $scope.flags.isTercero = false;
		  $scope.flags.isCuarto = false;
	  };  
      
	  $scope.pasoDos = (form) =>  {
		  
		 
		  if (form.$invalid) {
              showAlert.requiredFields(form);
              growl.error('Formulario incompleto', { ttl: 4000 });
              return;
          }else{
    		  $scope.flags.isPrimero = false;
    		  $scope.flags.isSegundo = true;
    		  $scope.flags.isTercero = false;
    		  $scope.flags.isCuarto = false;
          }

	  };  	  

	  $scope.pasoTres = () => {
		  $scope.flags.isPrimero = false;
		  $scope.flags.isSegundo = false;
		  $scope.flags.isTercero = true;
		  $scope.flags.isCuarto = false;
	  };  		  
	  
	  $scope.pasoCuatro = () =>{
		  $scope.flags.isPrimero = false;
		  $scope.flags.isSegundo = false;
		  $scope.flags.isTercero = false;
		  $scope.flags.isCuarto = true;
	  };
	  
	   
	    $scope.showModalAgignarApertura = function (data) {
       
       angular.copy(data, $scope.detailVO);
	        $scope.flags.isShowAsigApertura = true;
	        
	        $scope.flags.isPrimero = true;
	        $scope.flags.isSegundo = false;
	        $scope.flags.isTercero = false;
	        $scope.flags.isCuarto = false;
	        
	    };	  
	    
	    $scope.quitModalAgignarApertura = function () {
	    	 growl.success('Apertura Asignada correctamente', { ttl: 4000 });
	    	 $scope.flags.isShowAsigApertura = false;
	    	 $scope.flags.isPrimero = false;
	    	 $scope.flags.isSegundo = false;
	    	 debugger;
	         angular.copy($scope.detailVO);
	    };	
	    
	    $scope.showModalDatosPersonal = function (data) {
	    	$scope.flags. isShowDatosPersonales = true;
	        angular.copy(data, $scope.detailVO);
	 	        
	 	};
	 	
	 	$scope.quitModalDatosPersonal = function(){
	 		$scope.flags. isShowDatosPersonales = false;
	 	}
	 	
		$scope.printTicket = receit => {
			
			let ticket = document.getElementById(receit);
			let tickimp= window.open(' ','poimpr');
			
			tickimp.document.write(ticket.innerHTML);
			tickimp.document.close();
			tickimp.print();
			tickimp.close();
		
            $scope.quitModalAgignarApertura();
                	
		};
	    
});