angular.module(appTeclo).controller('depositoParcialController',
		function($scope,ModalService,depositoParcialService,growl,$rootScope,$document){
	
	 $scope.lists = {};
     $scope.searchVO = {typeSearch: [] ,value: undefined, viewMode:'status'};	
     
     onInit = () => {
    	 depositoParcialService.getTypeSearch()
		.success(lists=>{ $scope.lists.typeSearch = lists; $scope.searchVO.typeSearch = lists[0];})
		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});  
    	 
    	 depositoParcialService.getTypeMovimiento()
 		.success(lists=>{ $scope.lists.typeSearchMov = lists; $scope.searchVO.typeSearchMov = lists[0];})
 		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});    	
    	 
    	 depositoParcialService.getDatosMovimiento()
  		.success(lists=>{ $scope.lists.datoMovimiento = lists; $scope.searchVO.datoMovimiento = lists[0];})
  		.error(error => { growl.error('Error al cargar los tipos de búsqueda')});      	 
    	 
     };
     onInit();
});
	