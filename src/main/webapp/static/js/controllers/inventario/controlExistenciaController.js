angular.module(appTeclo).controller('controlExistenciaController',
    function ($scope, inventarioService, growl, storageService, jwtService, showAlert, $timeout, $filter) {

        $scope.searchVO = { typeSearch: new Object(), value: undefined, viewMode: 'status' };
        $scope.detailVO = new Object();
        $scope.lotDetailVO = new Object();
        $scope.requestVO = new Object({
            'nbSucursalMatriz': 'SUCURSAL MATRIZ REFORMA',
            'nbProducto': undefined,
            'tpPrioridadVO': undefined,
            'nbSolicitante': undefined,
            'txDescripcion': undefined
        });
        $scope.view = new Object();
        $scope.lists = {
            stockProduct: new Array(), listPriority: new Array(),
            stockProductTMP: new Array(), typeMeasurement: new Array(),
            productLot: new Array(), productLotTMP: new Array(), listProductRequest: new Array()
        };
        $scope.flags = {
            isMosaic: true, isList: false, isToasdRequest: false, isShowDetail: false, isLot: false, lotDetail: false,
            isDetailProduct: true, isDetailByLot: false, isRequestTab: false, isBtnEdit: false, isBtnNew: true
        };

        onInit = () => {
            inventarioService.getTypeSearch()
                .success(list => { $scope.lists.typeSearch = list; $scope.searchVO.typeSearch = list[0]; })
                .error(error => { growl.error('Error al cargar los tipos de búsqueda') });
            inventarioService.getTypeProduct()
                .success(list => { $scope.lists.typeProduct = list; $scope.searchVO.typeProduct = list[0]; })
                .error(error => { growl.error('Error al cargar los tipos de productos') });
            inventarioService.getStock()
                .success(list => { angular.copy(list, $scope.lists.stockProduct); angular.copy(list, $scope.lists.stockProductTMP); })
                .error(error => { growl.error('Error al cargar los productos: ' + error) });
            inventarioService.getPriority()
                .success(list => { angular.copy(list, $scope.lists.listPriority); })
                .error(error => { growl.error('Error al cargar las prioridades de solicitud: ' + error) });
            inventarioService.getTypeMeasurement()
                .success(list => { angular.copy(list, $scope.lists.typeMeasurement); })
                .error(error => { growl.error('Error al cargar las unidades de medida: ' + error) });
            inventarioService.getProductLot()
                .success(list => { angular.copy(list, $scope.lists.productLot); angular.copy(list, $scope.lists.productLotTMP); })
                .error(error => { growl.error('Error al cargar los lotes de productos: ' + error) });
        };
        $scope.toggleViewtab = function (tab) {
            $scope.view.searchSomething = undefined;
            switch (tab) {
                case 'm':
                    $scope.flags.isMosaic = true;
                    $scope.flags.isList = false;
                    $scope.flags.isRequestTab = false;
                    break;
                case 'l':
                    $scope.flags.isMosaic = false;
                    $scope.flags.isList = true;
                    $scope.flags.isRequestTab = false;
                    break;
                case 'lot':
                    $scope.flags.isMosaic = false;
                    $scope.flags.isList = false;
                    $scope.flags.isRequestTab = false;
                    break;
                case 'sol':
                    $scope.flags.isMosaic = false;
                    $scope.flags.isList = false;
                    $scope.flags.isRequestTab = true;
                    break;


            }
        };

        $scope.showModalRequest = function (data, type) {
            if (type === "EDIT") {
                $scope.requestVO = data;
                $scope.flags.isBtnEdit = true;
                $scope.flags.isBtnNew = false;
            } else if (type === "NEW") {
                $scope.requestVO.nbProducto = data.nbTipoProducto.toUpperCase();
                $scope.requestVO.nbSolicitante = getNbUserInSession().toUpperCase();
                $scope.flags.isBtnEdit = false;
                $scope.flags.isBtnNew = true;
            }
            /*$scope.flags = {
            isMosaic: true, isList: false, isToasdRequest: false, isShowDetail: false, isLot: false, lotDetail: false,
            isDetailProduct: true, isDetailByLot: false, isRequestTab: false, isBtnEdit: false, isBtnNew: true
        };*/
            $scope.flags.isToasdRequest = true;
        };

        $scope.quitModalRequest = function () {
            $scope.flags.isToasdRequest = false;
            $scope.requestVO = new Object({
                'nbSucursalMatriz': 'SUCURSAL MATRIZ REFORMA',
                'nbProducto': undefined,
                'tpPrioridadVO': undefined,
                'nbSolicitante': undefined,
                'txDescripcion': undefined
            });
        };

        $scope.showModalDetail = function (data) {
            angular.copy(data, $scope.detailVO);
            $scope.flags.isShowDetail = true;
        };

        $scope.quitModalDetail = function () {
            $scope.flags.isShowDetail = false;
            // angular.copy(setObject, $scope.detailVO);
        };

        function getNbUserInSession() {
            if (storageService.getToken())
                return jwtService.getNombreUsuario(storageService.getToken());
            return null;
        };

        $scope.addProduct = (form, requestVO) => {
            if (form.$invalid) {
                showAlert.requiredFields(form);
                growl.error('Formulario incompleto', { ttl: 4000 });
                return;
            } else {
                growl.success('La solcitud se agregó correctamente', { ttl: 4000 });
                $scope.lists.listProductRequest.push(requestVO);
                // debugger;
                $timeout(function () {
                    $scope.quitModalRequest();
                }, 800);
            }
        };


        $scope.filterProducts = function (typeSearch, typeProduct, valueInput) {
            let listFilter = new Array();
            if (typeSearch != null) {
                switch (typeSearch.cdTipoBusqueda) {
                    case 'TD':
                        angular.copy($scope.lists.stockProductTMP, $scope.lists.stockProduct);
                        break;
                    case 'TP':
                        for (let i = 0; i < $scope.lists.stockProductTMP.length; i++) {
                            if ($scope.lists.stockProductTMP[i].idTipoProducto === typeProduct.idTipoProducto) {
                                listFilter.push($scope.lists.stockProductTMP[i]);
                            }
                        }
                        if (listFilter.length > 0) {
                            angular.copy(listFilter, $scope.lists.stockProduct);
                        } else {
                            growl.error('No se encontraron coincidencias', { ttl: 4000 });
                            $scope.lists.stockProduct = new Array();
                        }
                        break;
                    case 'CP':
                        for (let i = 0; i < $scope.lists.stockProductTMP.length; i++) {
                            if ($scope.lists.stockProductTMP[i].flProducto === valueInput) {
                                listFilter.push($scope.lists.stockProductTMP[i]);
                            }
                        }
                        if (listFilter.length > 0) {
                            angular.copy(listFilter, $scope.lists.stockProduct);
                        } else {
                            growl.error('No se encontraron coincidencias', { ttl: 4000 });
                            $scope.lists.stockProduct = new Array();
                        }
                        break;
                    case 'NP':
                        $scope.lists.stockProduct = $filter('filter')($scope.lists.stockProductTMP, valueInput);
                        if ($scope.lists.stockProduct.length == 0) {
                            growl.error('No se encontraron coincidencias', { ttl: 4000 });
                            $scope.lists.stockProduct = new Array();
                        }
                        break;
                    case 'LOT':
                        $scope.flags.isLot = true;
                        $scope.flags.isMosaic = false;
                        $scope.flags.isList = false;
                        if (valueInput != null) {
                            $scope.lists.productLot = $filter('filter')($scope.lists.productLotTMP, valueInput);
                            if ($scope.lists.productLot.length < 1) {
                                growl.error('No se encontraron coincidencias', { ttl: 4000 });
                            }
                        } else {
                            angular.copy($scope.lists.productLotTMP, $scope.lists.productLot);
                        }
                        break;
                }
            } else {
                angular.copy($scope.lists.stockProductTMP, $scope.lists.stockProduct);
                //.success(list => { angular.copy(list, $scope.lists.stockProduct); angular.copy(list, $scope.lists.stockProductTMP);})
            }
        };

        $timeout(function () {
            $scope.slider = {
                value: 'Antiguos',
                options: {
                    showTicks: true,
                    stepsArray: 'Recientes.4 meses.8 meses.Antiguos'.split('.'),
                    showSelectionBar: true,
                    getSelectionBarColor: function (value) {
                        if (value == "Recientes" && $scope.searchVO.viewMode === 'antique') {
                            $scope.filterList(value);
                            $scope.view.searchSomething = value;
                            return '';
                        }
                        if (value == "4 meses" && $scope.searchVO.viewMode === 'antique') {
                            $scope.view.searchSomething = value;
                            $scope.filterList(value);
                            return '#B2EBF2';
                        }
                        if (value == "8 meses" && $scope.searchVO.viewMode === 'antique') {
                            $scope.view.searchSomething = value;
                            $scope.filterList(value);
                            return '#4DD0E1';
                        }
                        if (value == "Antiguos" && $scope.searchVO.viewMode === 'antique') {
                            $scope.view.searchSomething = value;
                            $scope.filterList(value);
                            return '#0091EA';
                        }
                        return '#2AE02A';
                    }
                }
            };
        }, 300);

        $scope.filterList = function (value) {
            $scope.oldValue = value;
        };

        $scope.modeOldWarehouse = function (nv, ov) {
            $scope.view.searchSomething = undefined;
            if (nv === 'antique') {
                $timeout(function () {
                    $scope.slider.value = 'Recientes'
                }, 200);
            } else {
                $timeout(function () {
                    $scope.slider.value = 'Antiguos';
                }, 200);
            }
            $('#btnAlmacen').trigger("click");
        };

        $scope.viewLotDetail = (data) => {
            //$scope.flags = { isMosaic: true, isList: false, isToasdRequest: false, isShowDetail: false, isLot: false, lotDetail: false };
            $scope.flags.lotDetail = true;
            angular.copy(data, $scope.lotDetailVO);
        };

        $scope.quitModalDetailLot = () => {
            $scope.flags.lotDetail = false;
            $scope.lotDetailVO = new Object();
        };


        $scope.updateValue = (model) => {
            //debugger;
            $scope.lists.stockProduct = new Array();
            $scope.lists.productLot = new Array();

            if (model.cdTipoBusqueda === 'LOT') {
                $scope.flags.isLot = true;
                $scope.flags.isMosaic = false;
                $scope.flags.isList = false;
                $scope.flags.isRequestTab = false;
            } else {
                $scope.flags.isLot = false;
                $scope.flags.isMosaic = true;
                $scope.flags.isList = false;
                $scope.flags.isRequestTab = false;
            }
            //console.info($scope.flags.isLot);
            //console.info(model);
        };

        $scope.toggleViewtabDetail = (tab) => {
            switch (tab) {
                case 'p':
                    $scope.flags.isDetailProduct = true;
                    $scope.flags.isDetailByLot = false;
                    break;
                case 'l':
                    $scope.flags.isDetailProduct = false;
                    $scope.flags.isDetailByLot = true;
                    break;
            }
        };

        $scope.sendAllRequest = (listRequest) => {
            if (listRequest.length > 0 && listRequest.length == 1) {
                growl.success('La solcitud fue enviada correctamente', { ttl: 4000 });
            } else if (listRequest.length > 0 && listRequest.length > 1) {
                growl.success('Las solcitudes fueron enviadas correctamente', { ttl: 4000 });
            }
        };

        $scope.notifyProductIsUpdated = () => {
            growl.success('Solicitud actulizada correctamente', { ttl: 3000 });
            $timeout(function () {
                $scope.quitModalRequest();
            }, 800);
        };

        $scope.showConfirm = (index) => {
            showAlert.confirmacion("¿Está seguro de quitar este pedido?", confirmDeleteRequest, index, cancelDeleteRequest);
        };

        confirmDeleteRequest = (index) => {
            $scope.lists.listProductRequest.splice(index, 1);
            $timeout(function () {
                if ($scope.lists.listProductRequest.length < 1) {
                    $scope.flags.isLot = false;
                    $scope.flags.isMosaic = true;
                    $scope.flags.isList = false;
                    $scope.flags.isRequestTab = false;
                }
            }, 500);
        };
        cancelDeleteRequest = () => {
            return;
        };

        onInit();
    });