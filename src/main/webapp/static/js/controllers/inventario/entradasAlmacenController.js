angular.module(appTeclo).controller('entradasAlmacenController',
    function ($scope, inventarioService, growl, storageService, jwtService, showAlert, $timeout) {

        let dateBefore = moment();//.add(-10, 'd');
        let dateAfter = moment().add(+10, 'd');

        $scope.flags = new Object({ isModalCancel: false });

        $scope.lists = { stockProduct: new Array(), stockProductTMP: new Array(), listOrdersPending: new Array() };

        //	Datetimepicker
        $scope.dateTimePickerOptions = new Object({
            format: 'DD/MM/YYYY hh:mm:ss',
            minDate: dateBefore,
            maxDate: dateAfter
        });

        $scope.entradaVO = new Object({
            'fhHraEntrada': undefined,
            'nbUsuarioRecibe': getNbUserInSession().toUpperCase(),
            'productoVO': new Array(), indexDelete: undefined, txMotivo: undefined
        });

        function getNbUserInSession() {
            if (storageService.getToken())
                return jwtService.getNombreUsuario(storageService.getToken());
            return null;
        };
        //fecha, precio, productos, importe
        onInit = () => {
            inventarioService.getStock()
                .success(list => { /* angular.copy(list, $scope.lists.stockProduct); */ angular.copy(list, $scope.lists.stockProductTMP); /* changeProductStock($scope.lists.stockProduct); */ })
                .error(error => { growl.error('Error al cargar los productos: ' + error) });
            inventarioService.getOrdersPending()
                .success(list => { angular.copy(list, $scope.lists.listOrdersPending); })
                .error(error => { growl.error('Error al cargar las solicitudes: ' + error) });
            $timeout(() => {
                $('#products').slimScroll({
                    height: '100%',
                    color: '#00243c',
                    opacity: .3,
                    size: "4px",
                    alwaysVisible: false
                });
            }, 100);

        };
        /*changeProductStock = (currentProductSctock) => {
            angular.forEach(currentProductSctock, function(value, key){
                value.nbTipoProducto = value.nbTipoProducto+' - ' + value.tpPresentacion;
             });
        };*/

        $scope.filterProducts = (filter) => {
            if (filter != null) {
                let idProducts = filter.idProducto.split(',');
                let arrayTMP = new Array();
                if (idProducts.length > 0) {
                    for (let i = 0; i < idProducts.length; i++) {
                        for (let j = 0; j < $scope.lists.stockProductTMP.length; j++) {
                            if (Number(idProducts[i]) === $scope.lists.stockProductTMP[j].idProducto) {
                                arrayTMP.push($scope.lists.stockProductTMP[j]);
                            }
                        }
                    }
                    if (arrayTMP.length > 0) {
                        angular.copy(arrayTMP, $scope.lists.stockProduct);

                        angular.forEach($scope.lists.stockProduct, function (current, index) {
                            current.nuCantidadRecib = generateQuantity(100, 400);
                            current.nuTotalImporte = generateAmmount(3000, 10000);
                        });
                    }
                }
            } else {
                $scope.lists.stockProduct = new Array();
            }
        };

        generateQuantity = (min, max) => {
            return Math.floor(Math.random() * max) + min;
        };
        generateAmmount = (min, max) => {
            return Math.floor(Math.random() * max) + min;
        };

        $scope.showModalCancel = (index) => {
            $scope.flags.isModalCancel = true;
            $scope.entradaVO.indexDelete = index;
        };

        $scope.quitModalCancel = () => {
            $scope.flags.isModalCancel = false;
            $scope.entradaVO.indexDelete = undefined;
            $scope.entradaVO.txMotivo = undefined;
        };

        $scope.confirmDelete = (form) => {
            if (form.$invalid) {
                showAlert.requiredFields(form);
                growl.error('Formulario incompleto', { ttl: 4000 });
                return;
            } else {
                if ($scope.entradaVO.indexDelete != undefined) {
                    growl.success('El prouducto se quitó correctamente de la solicitud', { ttl: 4000 });
                    $scope.lists.stockProduct.splice($scope.entradaVO.indexDelete, 1);
                    $timeout(function () {
                        $scope.quitModalCancel();
                    }, 800);
                } else {
                    growl.success('No se puede eliminar el elemento', { ttl: 4000 });
                    $timeout(function () {
                        $scope.quitModalCancel();
                    }, 800);
                }
            }
        };

        $scope.saveAll = () => {
            growl.success('Todo se guardó correctamente', { ttl: 4000 });
        };

        onInit();


    });
