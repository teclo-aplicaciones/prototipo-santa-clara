angular.module(appTeclo).controller('productosController',
    function ($scope, inventarioService, growl, storageService, jwtService, showAlert,
        $timeout, FileUploader) {

        var uploader = $scope.uploader = new FileUploader({
            url: this
        });

        $scope.pictureValidation = new Object({
            txWich: 'Seleccionar Archivo',
            txnumPictu: undefined
        });

        $scope.lists = { stock: new Array(), typeProduct: new Array(), typeMeasurement: new Array()};
        $scope.flags = { isNewProduct: false, isColapsed: false, isShowDetail: false, isDetailProduct: true, isDetailByPrice: false };
        $scope.newProductVO = new Object();
        $scope.detailVO = new Object();

        onInit = () => {
            inventarioService.getStock()
                .success(list => { angular.copy(list, $scope.lists.stock); })
                .error(error => { growl.error('Error al cargar los productos: ' + error) });
            inventarioService.getTypeProduct()
                .success(list => {  angular.copy(list, $scope.lists.typeProduct);})
                .error(error => { growl.error('Error al cargar los tipos de productos') });
            inventarioService.getTypeMeasurement()
                .success(list => { angular.copy(list, $scope.lists.typeMeasurement); })
                .error(error => { growl.error('Error al cargar las unidades de medida: ' + error) });

            $timeout(() => {
                $('#scrollProducts').slimScroll({
                    height: '100%',
                    color: '#00243c',
                    opacity: .3,
                    size: "4px",
                    alwaysVisible: false
                });
            }, 100);

            $timeout(() => {
                $('#boxTitle').slimScroll({
                    height: '100%',
                    color: '#00243c',
                    opacity: .3,
                    size: "4px",
                    alwaysVisible: false
                });
            }, 100);

        };

        $scope.showFormNewProduct = () => {
            $scope.flags.isNewProduct = true;
        };


        $scope.quitFormNewProduct = () => {
            $scope.flags.isNewProduct = false;
            $scope.uploader.queue = new Array();
        };

        $scope.productoPersist = (form,data) => {
            if (form.$invalid) {
                showAlert.requiredFields(form);
                growl.error('Formulario incompleto', { ttl: 4000 });
                return;
            } else {
                growl.success('El producto se registró correctamente', { ttl: 4000 });
                $timeout(function () {
                    $scope.quitFormNewProduct();
                }, 1000);
            }
        };

        $scope.$watch("uploader.queue.length", function (newVal, olvVal) {
            if (newVal == 0) {
                $scope.pictureValidation.txWich = " Seleccionar Archivo";
            } else {
                $scope.pictureValidation.txnumPictu = newVal + " archivos seleccionados";
            }
        });

        $scope.showModalPicture = (type) => {
            //showModalPicture
            //let div = document.getElementById('showModalPicture');
            switch (type) {
                case 'S':
                    $scope.flags.isColapsed = true;
                    break;
                case 'H':
                    $scope.flags.isColapsed = false;
                    break;
            };
        };

        $scope.showDetailproduct = (data) => {
            angular.copy(data, $scope.detailVO);
            $scope.flags.isShowDetail = true;
        };

        $scope.quitModalDetail = () => {
            $scope.flags.isShowDetail = false;
        };



        $scope.toggleViewtabDetail = (tab) => {
            switch (tab) {
                case 'p':
                    $scope.flags.isDetailProduct = true;
                    $scope.flags.isDetailByPrice = false;
                    break;
                case 'pr':
                    $scope.flags.isDetailProduct = false;
                    $scope.flags.isDetailByPrice = true;
                    break;
            }
        };



        onInit();
    });