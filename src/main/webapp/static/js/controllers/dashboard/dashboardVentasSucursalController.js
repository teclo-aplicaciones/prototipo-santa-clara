angular.module(appTeclo).controller('dashboardVentasSucursalController', function($scope, $filter, $timeout) {
	$scope.ctrl={
		valueUnidadesActivas:1,
		unidadesActivas:90,
		valueUnidadesMantenimiento:1,
		unidadesMantenimiento:7,
		valueUnidadesDetenidos:1,
		unidadesDetenidos:3,
		duration:1000,
		effect:'swing'	
	};
	$scope.init = {
		zAnio1 : 2019,
		zMes1 : 3,
		zAnio2 : 2019,
		zMes2 : 3,
		zSuc2 : 1,
		zAnio3 : 2019,
		zMes3 : 3,
		zSuc3 : 1
	};
	$scope.comboSucursal = [
		{"idTipoSucursal": 1, "cdTipoSucursal": "Polanco"},
		{"idTipoSucursal": 2, "cdTipoSucursal": "Centro Histórico"},
		{"idTipoSucursal": 3, "cdTipoSucursal": "Miramontes"},
		{"idTipoSucursal": 4, "cdTipoSucursal": "Perisur"},
		{"idTipoSucursal": 5, "cdTipoSucursal": "Azcapotzalco"}
	];
	
	$scope.comboMes = [
		{"idMes":1,"descMes":"Enero"},
		{"idMes":2,"descMes":"Febrero"},
		{"idMes":3,"descMes":"Marzo"},
		{"idMes":4,"descMes":"Abril"},
		{"idMes":5,"descMes":"Mayo"},
		{"idMes":6,"descMes":"Junio"},
		{"idMes":7,"descMes":"Julio"},
		{"idMes":8,"descMes":"Agosto"},
		{"idMes":9,"descMes":"Septiembre"},
		{"idMes":10,"descMes":"Octubre"},
		{"idMes":11,"descMes":"Noviembre"},
		{"idMes":12,"descMes":"Diciembre"}
	];
	
	$scope.comboAnio = [
    	{"idAnio":2017,"descAnio":"2017"},
    	{"idAnio":2018,"descAnio":"2018"},
    	{"idAnio":2019,"descAnio":"2019"}
    ];
	
	$scope.ctrls = [
		{myValue:1, nbPanel: "Ventas",				myTarget:52,	myDuration:1500, bgPanel:'bg-blue',		icon: "fa-line-chart",		myEffect:'swing'},
		{myValue:2, nbPanel: "Pedidos Pendientes",	myTarget:8,		myDuration:1500, bgPanel:'bg-yellow',	icon: "fa-shopping-cart",	myEffect:'swing'},
		{myValue:3, nbPanel: "Pedidos Realizados",	myTarget:35,	myDuration:1500, bgPanel:'bg-green',	icon: "fa-shopping-cart",	myEffect:'swing'},
		{myValue:4, nbPanel: "Pedidos Cancelados",	myTarget:10,	myDuration:1500, bgPanel:'bg-red',		icon: "fa-shopping-cart",	myEffect:'swing'}
	];
	
	$scope.Top10MasVen = [
	    {
	    	"orden": 1,
	        "idProducto": 1,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Entera",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112030",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheEntera1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "tpUnidadMedida":"Pieza",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheEntera1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheEntera1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheEntera1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 2,
	        "idProducto": 2,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Ligth",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112031",
	        "nuStock": 48,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheLigth1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheLigth1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 3,
	        "idProducto": 3,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Semidescremada",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112032",
	        "nuStock": 101,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 4,
	        "idProducto": 4,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Deslactosada Litgth",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112033",
	        "nuStock": 41,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 5,
	        "idProducto": 5,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Deslactosada",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112034",
	        "nuStock": 89,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 6,
	        "idProducto": 6,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Leche de Chocolate",
	        "tpPresentacion": "250ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112035",
	        "nuStock": 99,
	        "nuStockMax": 150,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/lecheChocolate250.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate250.PNG" }
	        ]
	    },
	    {
	    	"orden": 7,
	        "idProducto": 7,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Leche de Fresa",
	        "tpPresentacion": "250ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112036",
	        "nuStock": 50,
	        "nuStockMax": 300,
	        "nuStockMin": 100,
	        "lbImg": "static/dist/img/imgProducto/lecheFresa250.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheFresa250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa250.PNG" }
	        ]
	    },
	    {
	    	"orden": 8,
	        "idProducto": 8,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Leche de Menta con Chocolate",
	        "tpPresentacion": "250ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112037",
	        "nuStock": 86,
	        "nuStockMax": 300,
	        "nuStockMin": 45,
	        "lbImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG" }
	        ]
	    },
	    {
	    	"orden": 9,
	        "idProducto": 9,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Chocolate",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112038",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/lecheChocolate200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate200.PNG" }
	        ]
	    },
	    {
	    	"orden": 10,
	        "idProducto": 10,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Fresa",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112039",
	        "nuStock": 22,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/lecheFresa200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheFresa200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa200.PNG" }
	        ]
	    }
	];
	
	$scope.Top10MenosVen = [
	    {
	    	"orden": 1,
	        "idProducto": 11,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Cajeta",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112040",
	        "nuStock": 90,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheCajeta200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheCajeta200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCajeta200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCajeta200.PNG" }
	        ]
	    },
	    {
	    	"orden": 2,
	        "idProducto": 12,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Capuccino",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112041",
	        "nuStock": 87,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG" }
	        ]
	    },
	    {
	    	"orden": 3,
	        "idProducto": 13,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Vainilla",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112042",
	        "nuStock": 156,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheVainilla200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheVainilla200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheVainilla200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheVainilla200.PNG" }
	        ]
	    },
	    {
	    	"orden": 4,
	        "idProducto": 14,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Mango",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112043",
	        "nuStock": 92,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/yogurtMango125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtMango125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMango125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMango125.PNG" }
	        ]
	    },
	    {
	    	"orden": 5,
	        "idProducto": 15,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Limon",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112044",
	        "nuStock": 92,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/yogurtLimon125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtLimon125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtLimon125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtLimon125.PNG" }
	        ]
	    },
	    {
	    	"orden": 6,
	        "idProducto": 16,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Guayaba",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112045",
	        "nuStock": 92,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG" }
	        ]
	    },
	    {
	    	"orden": 7,
	        "idProducto": 17,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Miel",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112046",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 20,
	        "lbImg": "static/dist/img/imgProducto/yogurtMiel200.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtMiel200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMiel200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMiel200.PNG" }
	        ]
	    },
	    {
	    	"orden": 8,
	        "idProducto": 18,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Mamey",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112047",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 20,
	        "lbImg": "static/dist/img/imgProducto/yogurtMamey200.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtMamey200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMamey200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMamey200.PNG" }
	        ]
	    },
	    {
	    	"orden": 9,
	        "idProducto": 19,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Durazno",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112048",
	        "nuStock": 55,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG" }
	        ]
	    },
	    {
	    	"orden": 10,
	        "idProducto": 20,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Manzana",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112049",
	        "nuStock": 59,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/yogurtManzana125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtManzana125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtManzana125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtManzana125.PNG" }
	        ]
	    }
	];
	
	var dataGraficaSalidas = 
	[
		  {"tipoSalida": "Ventas", "value": 250},
		  {"tipoSalida": "Promociones","value": 50},
		  {"tipoSalida": "Devolucion a proveedór","value": 20},
		  {"tipoSalida": "Merma","value": 15},
		  {"tipoSalida": "Traspaso entre almacenes","value": 45},
		  {"tipoSalida": "Cortesia","value": 30}
	];
	
	var dataSucursales = [
		{
			"idTipoSucursal": 1,
			"cdTipoSucursal": "Polanco",
			"nbTipoSucursal": " Helados Santa Clara Polanco, CDMX",
			"numTelefono": "01 55 5280 5122",
			"correo": "sanclarasuc01@santaclara.com.mx",
			"direccion": "Av. Pdte. Masaryk 194, Polanco, Polanco V Secc, 11560 Ciudad de M�xico, CDMX",
			"lbImg": "static/dist/img/imgSucursales/santaclara1.jpg",
			"tpImg":"median",
			"color1": "#5b0218",
			"color2": "#d3d3d1",
			"ingresos":50,
			"ganancias":120
		},
		{
			"idTipoSucursal": 2,
			"cdTipoSucursal": "Centro Histórico",
			"nbTipoSucursal": "Helados Santa Clara Centro Hist�rico, Centro,CDMX",
			"numTelefono": "01 55 5280 7822",
			"correo": "sanclarasuc02@santaclara.com.mx",
			"direccion": "Av Francisco I. Madero 56, Centro Hist�rico, Centro, 06000 Ciudad de M�xico, CDMX",
			"lbImg": "static/dist/img/imgSucursales/santaclara2.jpg",
			"tpImg":"median",
			"color1": "#5b0218",
			"color2": "#d3d3d1",
			"ingresos":100,
			"ganancias":180
		},
		{
			"idTipoSucursal": 3,
			"cdTipoSucursal": "Miramontes",
			"nbTipoSucursal": "Helados Santa Clara Miramontes, Coyoac�n,CDMX ",
			"numTelefono": "01 55 5280 5123",
			"correo": "sanclarasuc03@santaclara.com.mx",
			"direccion": "Av. Miguel A.de Quevedo No.486 Col., Coyoac�n, 04000 Coyoac�n, CDMX",
			"lbImg": "static/dist/img/imgSucursales/santaclara3.jpg",
			"tpImg":"median",
			"color1": "#5b0218",
			"color2": "#d3d3d1",
			"ingresos":110,
			"ganancias":280
		},
		{
			"idTipoSucursal": 4,
			"cdTipoSucursal": "Perisur",
			"nbTipoSucursal": "Helados Santa Clara Perisur, CDMX ",
			"numTelefono": "01 55 5280 1121",
			"correo": "sanclarasuc04@santaclara.com.mx",
			"direccion": "Perisur, Dentro de Walmart, 02770 Ciudad de M�xico, CDMX",
			"lbImg": "static/dist/img/imgSucursales/santaclara4.jpg",
			"tpImg":"median",
			"color1": "#5b0218",
			"color2": "#d3d3d1",
			"ingresos":150,
			"ganancias":80
		},
		{
			"idTipoSucursal": 5,
			"cdTipoSucursal": "Azcapotzalco",
			"nbTipoSucursal": "Helados Santa Clara Azcapotzalco, CDMX",
			"numTelefono": "01 55 5280 5673",
			"correo": "sanclarasuc05@santaclara.com.mx",
			"direccion": "Camino a Nextengo 78, Santa Cruz Acayucan, Dentro de Walmart, Santa Apolonia, 02770 Ciudad de M�xico, CDMX",
			"lbImg": "static/dist/img/imgSucursales/santaclara1.jpg",
			"tpImg":"median",
			"color1": "#5b0218",
			"color2": "#d3d3d1",
			"ingresos":100,
			"ganancias":300
	    }
	];
	
	var dataVentasXSuc = [
		{"mes":"Enero","ventas":254},
		{"mes":"Febrero","ventas":308},
		{"mes":"Marzo","ventas":100},
		{"mes":"Abril","ventas":250},
		{"mes":"Mayo","ventas":234},
		{"mes":"Junio","ventas":380},
		{"mes":"Julio","ventas":200},
		{"mes":"Agosto","ventas":328},
		{"mes":"Septiembre","ventas":105},
		{"mes":"Octubre","ventas":180},
		{"mes":"Noviembre","ventas":259},
		{"mes":"Diciembre","ventas":80}
	];
	
	iniciaTop10 = function(){
		$scope.arrayTop10MasVen = $scope.Top10MasVen;
		$scope.arrayTop10MenosVen = $scope.Top10MenosVen;
	}
	
	graficaSalidasProductos = function(){
		var graficaSalidas = AmCharts.makeChart( "graficaSalidas", {
			"type": "pie",
			"theme": "donaMantto",
			"dataProvider": dataGraficaSalidas,
			"titleField": "tipoSalida",
			"valueField": "value",
			"labelRadius": 5,
		    "marginTop": 3,
		    "marginRight": 0,
		    "marginLeft": 80,
		    "marginBottom": 0,
			"radius": "42%",
			"innerRadius": "60%",
			"labelText": "[[title]]",
			"export": {
				"enabled": true
			}
		});
	}
	
	graficaVentasXSucursal = function() {
		var graficaVentasXSucursal = AmCharts.makeChart("graficaVentasXSucursal", {
		    "theme": "light",
		    "type": "serial",
		    "dataProvider": dataVentasXSuc,
		    "valueAxes": [{
		        "title": ""
		    }],
		    "graphs": [{
		        "balloonText": "[[category]]: [[ventas]]",
		        "fillAlphas": 1,
		        "lineAlpha": 0.2,
		        "title": "Income",
		        "type": "column",
		        "valueField": "ventas"
		    }],
		    "depth3D": 8,
		    "angle": 20,
		    "rotate": true,
		    "categoryField": "mes",
		    "categoryAxis": {
		        "gridPosition": "start",
				"gridAlpha": 0.05,
		        "fillAlpha": 0.05,
		        "position": "left"
		    },
		    "export": {
		    	"enabled": true
		     }
		});
	}
	
	graficaIngGanSucursal = function(){
		var graficaIngGanSucursal = AmCharts.makeChart("graficaIngGanSucursal", {
		    "theme": "none",
		    "type": "serial",
		    "dataProvider": dataSucursales,
		    "valueAxes": [{
		        "position": "left",
		        "title": "Ingresos y Ganancias"
		    }],
		    "startDuration": 1,
		    "graphs": [{
		        "balloonText": "<span style='font-size:13px;'>[[nbTipoSucursal]]: <b>[[value]]</b></span>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "Ingresos",
		        "type": "column",
		        "valueField": "ingresos",
		        "colorField": "color1",
		        "cornerRadiusTop": 8
		    }, {
		        "balloonText": "<span style='font-size:13px;'>[[nbTipoSucursal]]: <b>[[value]]</b></span>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "Ganancias",
		        "type": "column",
		        "clustered":false,
		        "columnWidth":0.5,
		        "valueField": "ganancias",
		        "bulletOffset": 10,
		        "bulletSize": 52,
		        "colorField": "color2",
		        "cornerRadiusTop": 8,
		        "customBulletField": "lbImg"
		    }],
		    "legend": {
		        "horizontalGap": 10,
		        "maxColumns": 1,
		        "position": "right",
		        "useGraphSettings": true,
		        "useMarkerColorForLabels": true,
		        "markerSize": 10
		    },
		    "plotAreaFillAlphas": 0.1,
		    "categoryField": "cdTipoSucursal",
		    "categoryAxis": {
		        "gridPosition": "start",
		        "labelRotation": 20
		    },
		    "export": {
		    	"enabled": true
		     }
		});
	}
	
	graficaMetasXSucursal = function(){
		var graficaMetasXSucursal = AmCharts.makeChart("graficaMetasXSucursal", {
			"theme": "none",
			"type": "gauge",
			"axes": [{
				"topTextFontSize": 20,
				"topTextYOffset": 100,
				"axisColor": "#5b0218",
				"axisThickness": 1,
				"endValue": $scope.dataMetasSuc.metaSuc,
				"gridInside": true,
				"inside": true,
				"radius": "60%",
				"valueInterval": $scope.dataMetasSuc.intervalos,
				"tickColor": "#5b0218",
				"startAngle": -90,
				"endAngle": 90,
				//"unit": "%",
				"bandOutlineAlpha": 0,
				"topText": $scope.dataMetasSuc.textoCentral,
				"bands": [{
					"color": "#5b0218",
					"endValue": $scope.dataMetasSuc.metaSuc,
					"innerRadius": "105%",
					"radius": "170%",
					"gradientRatio": [0.5, 0, -0.5],
					"startValue": 0
				}, {
					"color": "#d3d3d1",
					"endValue": $scope.dataMetasSuc.ventas,
					"innerRadius": "105%",
					"radius": "170%",
					"gradientRatio": [0.5, 0, -0.5],
					"startValue": 0
				}]
			}],
			"arrows": [{
				"alpha": 1,
				"innerRadius": "35%",
				"nailRadius": 0,
				"radius": "170%",
				"value": $scope.dataMetasSuc.ventas
			}]
		});
		
//		var metaSuc = 1800;
//		var intervalos = metaSuc / 10;
//		var ventas = 670;
//		var textoCentral = ventas;
//		
//		var graficaMetasXSucursal = AmCharts.makeChart("graficaMetasXSucursal", {
//		  "theme": "none",
//		  "type": "gauge",
//		  "axes": [{
//		    "topTextFontSize": 20,
//		    "topTextYOffset": 100,
//		    "axisColor": "#5b0218",
//		    "axisThickness": 1,
//		    "endValue": metaSuc,
//		    "gridInside": true,
//		    "inside": true,
//		    "radius": "60%",
//		    "valueInterval": intervalos,
//		    "tickColor": "#5b0218",
//		    "startAngle": -90,
//		    "endAngle": 90,
//		    //"unit": "%",
//		    "bandOutlineAlpha": 0,
//		    "topText": textoCentral,
//		    "bands": [{
//		      "color": "#5b0218",
//		      "endValue": metaSuc,
//		      "innerRadius": "105%",
//		      "radius": "170%",
//		      "gradientRatio": [0.5, 0, -0.5],
//		      "startValue": 0
//		    }, {
//		      "color": "#d3d3d1",
//		      "endValue": ventas,
//		      "innerRadius": "105%",
//		      "radius": "170%",
//		      "gradientRatio": [0.5, 0, -0.5],
//		      "startValue": 0
//		    }]
//		  }],
//		  "arrows": [{
//		    "alpha": 1,
//		    "innerRadius": "35%",
//		    "nailRadius": 0,
//		    "radius": "170%",
//		    "value": ventas
//		  }]
//		});
	}
	
	$scope.tabMasV = true;
	$scope.tabMenosV = false;
	$scope.tabSucVen = true;
	$scope.tabSucIng = false;
	
	$scope.tabsTOP10 = [
   		{idTab: 1, nbTab: 'Más Vendidos', isActive: true},
   		{idTab: 2, nbTab: 'Menos Vendidos', isActive: false}
   	];
	
	$scope.tabsSucursales = [
   		{idTab: 1, nbTab: 'Ventas', isActive: true},
   		{idTab: 2, nbTab: 'Ingresos', isActive: false}
   	];
	
	iniciaGrafMetas = function(){
		var metaSuc = 1800;
		var intervalos = metaSuc / 10;
		var ventas = 670;
		var textoCentral = "Ingresos por venta: $"+ ventas;
		
		$scope.dataMetasSuc = {
			metaSuc : 1800,
			intervalos : (metaSuc / 10),
			ventas : 670,
			textoCentral : ventas
		};
		$timeout(function() {
			graficaMetasXSucursal();
		});
	}
	
	$scope.changeGraph = function(tab) {
		switch (tab) {
			case 'Menos Vendidos':
				$scope.tabsTOP10[0].isActive = false;
				$scope.tabsTOP10[1].isActive = true;
				$scope.tabMenosV = true;
				$scope.tabMasV = false;
				break;
			case 'Más Vendidos':
				$scope.tabsTOP10[1].isActive = false;
				$scope.tabsTOP10[0].isActive = true;
				$scope.tabMenosV = false;
				$scope.tabMasV = true;
				break;
			case 'Ingresos':
				$scope.tabsSucursales[0].isActive = false;
				$scope.tabsSucursales[1].isActive = true;
				$scope.tabSucIng = true;
				$scope.tabSucVen = false;
				$timeout(function() {
					graficaIngGanSucursal();
				});
				break;
			case 'Ventas':
				$scope.tabsSucursales[1].isActive = false;
				$scope.tabsSucursales[0].isActive = true;
				$scope.tabSucIng = false;
				$scope.tabSucVen = true;
				$timeout(function() {
					graficaVentasXSucursal();
				});
				$timeout(function() {
					graficaMetasXSucursal();
				});
				break;
			default:
				break;
		}
	}
	
	initController = function() {
		$scope.dataMetasSuc = {};
		iniciaTop10();
		$timeout(function() {
			graficaSalidasProductos();
		});
		$timeout(function() {
			graficaVentasXSucursal();
		});
		$timeout(function() {
			//graficaMetasXSucursal();
			iniciaGrafMetas();
		});
	}
	
	initController();
});