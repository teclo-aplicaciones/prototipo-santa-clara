angular.module(appTeclo).controller('dashboardProveedoresController',function($scope, $filter, $timeout, ModalService) {

	$scope.suppliers = [];
	$scope.filter = {};
	$scope.init = {
		zAnio1 : 2019,
		zMes1 : 3,
		zAnio2 : 2019,
		zMes2 : 3,
		zSuc2 : 1,
		zAnio3 : 2019,
		zMes3 : 3,
		zSuc3 : 1
	};
	
	$scope.comboSucursal = [
		{"idTipoSucursal": 1, "cdTipoSucursal": "Polanco"},
		{"idTipoSucursal": 2, "cdTipoSucursal": "Centro Histórico"},
		{"idTipoSucursal": 3, "cdTipoSucursal": "Miramontes"},
		{"idTipoSucursal": 4, "cdTipoSucursal": "Perisur"},
		{"idTipoSucursal": 5, "cdTipoSucursal": "Azcapotzalco"}
	];
	
	$scope.comboMes = [
		{"idMes":1,"descMes":"Enero"},
		{"idMes":2,"descMes":"Febrero"},
		{"idMes":3,"descMes":"Marzo"},
		{"idMes":4,"descMes":"Abril"},
		{"idMes":5,"descMes":"Mayo"},
		{"idMes":6,"descMes":"Junio"},
		{"idMes":7,"descMes":"Julio"},
		{"idMes":8,"descMes":"Agosto"},
		{"idMes":9,"descMes":"Septiembre"},
		{"idMes":10,"descMes":"Octubre"},
		{"idMes":11,"descMes":"Noviembre"},
		{"idMes":12,"descMes":"Diciembre"}
	];
	
	$scope.comboAnio = [
    	{"idAnio":2017,"descAnio":"2017"},
    	{"idAnio":2018,"descAnio":"2018"},
    	{"idAnio":2019,"descAnio":"2019"}
    ];
	
	$scope.ctrls = [
		{myValue:1, nbPanel: "Ordenes",   nbPanel2: "Pendientes",  myTarget:8,   myDuration:1500, bgPanel:'bg-yellow', icon: "fa-list-ol", myEffect:'swing'},
		{myValue:2, nbPanel: "Ordenes",   nbPanel2: "Entregadas",  myTarget:17, myDuration:1500, bgPanel:'bg-green',  icon: "fa-list-ol", myEffect:'swing'},
		{myValue:3, nbPanel: "Ordenes",   nbPanel2: "Canceladas",  myTarget:3, myDuration:1500, bgPanel:'bg-red',    icon: "fa-list-ol", myEffect:'swing'},
		{myValue:4, nbPanel: "Productos", nbPanel2: "registrados", myTarget:10, myDuration:1500, bgPanel:'bg-blue',   icon: "fa-list-ol", myEffect:'swing'}
	];
	

	$scope.Top10MasVen = [
	    {
	    	"orden": 1,
	        "idProducto": 1,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Entera",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112030",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheEntera1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "tpUnidadMedida":"Pieza",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheEntera1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheEntera1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheEntera1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 2,
	        "idProducto": 2,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Ligth",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112031",
	        "nuStock": 48,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheLigth1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheLigth1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 3,
	        "idProducto": 3,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Semidescremada",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112032",
	        "nuStock": 101,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheSemidescremada1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 4,
	        "idProducto": 4,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Deslactosada Litgth",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112033",
	        "nuStock": 41,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosadaLigth1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 5,
	        "idProducto": 5,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Santa Clara Deslactosada",
	        "tpPresentacion": "1L" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112034",
	        "nuStock": 89,
	        "nuStockMax": 200,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheDeslactosada1l.PNG" }
	        ]
	    },
	    {
	    	"orden": 6,
	        "idProducto": 6,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Leche de Chocolate",
	        "tpPresentacion": "250ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112035",
	        "nuStock": 99,
	        "nuStockMax": 150,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/lecheChocolate250.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate250.PNG" }
	        ]
	    },
	    {
	    	"orden": 7,
	        "idProducto": 7,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Leche de Fresa",
	        "tpPresentacion": "250ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112036",
	        "nuStock": 50,
	        "nuStockMax": 300,
	        "nuStockMin": 100,
	        "lbImg": "static/dist/img/imgProducto/lecheFresa250.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheFresa250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa250.PNG" }
	        ]
	    },
	    {
	    	"orden": 8,
	        "idProducto": 8,
	        "idTipoProducto": 5,
	        "nbTipoProducto": "Leche de Menta con Chocolate",
	        "tpPresentacion": "250ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112037",
	        "nuStock": 86,
	        "nuStockMax": 300,
	        "nuStockMin": 45,
	        "lbImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheMentaChocolate250.PNG" }
	        ]
	    },
	    {
	    	"orden": 9,
	        "idProducto": 9,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Chocolate",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"Recientes",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112038",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/lecheChocolate200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheChocolate200.PNG" }
	        ]
	    },
	    {
	    	"orden": 10,
	        "idProducto": 10,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Fresa",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112039",
	        "nuStock": 22,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/lecheFresa200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheFresa200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheFresa200.PNG" }
	        ]
	    }
	];
	
	$scope.Top10MenosVen = [
	    {
	    	"orden": 1,
	        "idProducto": 11,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Cajeta",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112040",
	        "nuStock": 90,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheCajeta200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheCajeta200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCajeta200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCajeta200.PNG" }
	        ]
	    },
	    {
	    	"orden": 2,
	        "idProducto": 12,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Capuccino",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112041",
	        "nuStock": 87,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheCapuccino200.PNG" }
	        ]
	    },
	    {
	    	"orden": 3,
	        "idProducto": 13,
	        "idTipoProducto": 10,
	        "nbTipoProducto": "Leche de Vainilla",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112042",
	        "nuStock": 156,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/lecheVainilla200.PNG",
	        "tpImg":"median",
	        "nuPrecio": "23.50",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/lecheVainilla200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheVainilla200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/lecheVainilla200.PNG" }
	        ]
	    },
	    {
	    	"orden": 4,
	        "idProducto": 14,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Mango",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112043",
	        "nuStock": 92,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/yogurtMango125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtMango125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMango125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMango125.PNG" }
	        ]
	    },
	    {
	    	"orden": 5,
	        "idProducto": 15,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Limon",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112044",
	        "nuStock": 92,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/yogurtLimon125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtLimon125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtLimon125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtLimon125.PNG" }
	        ]
	    },
	    {
	    	"orden": 6,
	        "idProducto": 16,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Guayaba",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Suficiente",
	        "flProducto":"FOL_2019112045",
	        "nuStock": 92,
	        "nuStockMax": 300,
	        "nuStockMin": 50,
	        "lbImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtGuayaba125.PNG" }
	        ]
	    },
	    {
	    	"orden": 7,
	        "idProducto": 17,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Miel",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112046",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 20,
	        "lbImg": "static/dist/img/imgProducto/yogurtMiel200.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtMiel200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMiel200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMiel200.PNG" }
	        ]
	    },
	    {
	    	"orden": 8,
	        "idProducto": 18,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Mamey",
	        "tpPresentacion": "200ML" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Agotado",
	        "flProducto":"FOL_2019112047",
	        "nuStock": 0,
	        "nuStockMax": 200,
	        "nuStockMin": 20,
	        "lbImg": "static/dist/img/imgProducto/yogurtMamey200.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtMamey200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMamey200.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtMamey200.PNG" }
	        ]
	    },
	    {
	    	"orden": 9,
	        "idProducto": 19,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Durazno",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112048",
	        "nuStock": 55,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtDurazno125.PNG" }
	        ]
	    },
	    {
	    	"orden": 10,
	        "idProducto": 20,
	        "idTipoProducto": 7,
	        "nbTipoProducto": "Yogurt de Manzana",
	        "tpPresentacion": "125G" ,
	        "nbAlmacen": "Almacén 1","nbAntigueda":"4 meses",
	        "txDescripcion":"Descricion",
	        "nbStatus":"Por Agotarse",
	        "flProducto":"FOL_2019112049",
	        "nuStock": 59,
	        "nuStockMax": 200,
	        "nuStockMin": 30,
	        "lbImg": "static/dist/img/imgProducto/yogurtManzana125.PNG",
	        "tpImg":"small",
	        "nuPrecio": "18.00",
	        "listimages": [
	            { "blImg": "static/dist/img/imgProducto/yogurtManzana125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtManzana125.PNG" },
	            { "blImg": "static/dist/img/imgProducto/yogurtManzana125.PNG" }
	        ]
	    }
	];
	
	iniciaTop10 = function(){
		$scope.arrayTop10MasVen = $scope.Top10MasVen;
		$scope.arrayTop10MenosVen = $scope.Top10MenosVen;
	}
	
	var dataVentasXSuc = [
		{
			"mes":"Enero",
			"ventas":254,
			"ingresos":1500,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Febrero",
			"ventas":308,
			"ingresos":2150,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Marzo",
			"ventas":100,
			"ingresos":970,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Abril",
			"ventas":250,
			"ingresos":1850,
		  "color1": "#D6D3D4",
		  "color2": "#5b0218"
		}, {
			"mes":"Mayo",
			"ventas":234,
			"ingresos":1250,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Junio",
			"ventas":380,
			"ingresos":3550,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Julio",
			"ventas":200,
			"ingresos":2150,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Agosto",
			"ventas":328,
			"ingresos":3560,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Septiembre",
			"ventas":105,
			"ingresos":1498,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Octubre",
			"ventas":180,
			"ingresos":1234,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Noviembre",
			"ventas":259,
			"ingresos":2345,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}, {
			"mes":"Diciembre",
			"ventas":80,
			"ingresos":570,
			"color1": "#D6D3D4",
			"color2": "#5b0218"
		}
		    //, {
		    //    "date": "2012-01-13",
		    //    "townName": "Salt Lake City",
		    //    "townSize": 12,
		    //    "distance": 425,
		    //    "duration": 670,
		    //    "latitude": 40.75,
		    //    "dashLength": 8,
		    //    "alpha": 0.4
		    //}
	];
	
	var dataTiemposEntrega = [
		{
			"etiqueta" : "Anticipados \n<= 1 Día",
			"descripcion" : "<= 1 Día",
			"entregas" : 10
		},
		{
			"etiqueta" : "Leve Retraso \n> 1 Día a 2 Días",
			"descripcion" : "> 1 Día a 2 Días",
			"entregas" : 5
		},
		{
			"etiqueta":"Con Retraso \n> 2 Días",
			"descripcion": "> 2 Días",
			"entregas": 2
		}
	];
	
	graficaVentasXSucursal = function(){
		var graficaVentasXSucursal = AmCharts.makeChart("graficaVentasXSucursal", {
			"type": "serial",
			"theme": "none",
			"legend": {
				"equalWidths": false,
				"useGraphSettings": true,
				"valueAlign": "left",
				"valueWidth": 120
			},
			"dataProvider": dataVentasXSuc,
			"valueAxes": [
				{
					"id": "ventasAxis",
					"axisAlpha": 0,
					"gridAlpha": 0,
					"position": "left",
					"title": "ventas"
				}, {
			        "id": "ingresosAxis",
			        "axisAlpha": 0,
			        "gridAlpha": 0,
			        //"inside": false,
			        "position": "right",
			        "title": "Ingresos"
			    }
			],
			"graphs": [
				{
					"alphaField": "alpha",
					"balloonText": "[[value]] Ventas",
					"dashLengthField": "dashLength",
					"fillAlphas": 0.7,
					"legendPeriodValueText": "Total: [[value.sum]]",
					"legendValueText": "[[value]] Ventas [[mes]]",
					"title": "Ventas",
					"type": "column",
					"valueField": "ventas",
					"valueAxis": "distanceAxis",
					//"lineAlpha": 2,
					"lineColor": "#5b0218",
					"colorField": "color1"
				}, 
				{
					"balloonText": "Ingresos: [[value]]",
					"bullet": "round",
					"bulletBorderAlpha": 1,
					"useLineColorForBulletBorder": true,
					"bulletColor": "#FFFFFF",
					"lineColor": "#5b0218",
	//		        "bulletSizeField": "townSize",
					"dashLengthField": "dashLength",
					"descriptionField": "ingresos",
					"labelPosition": "right",
					"labelText": "[[ingresos]]",
					"legendValueText": "[[value]]/[[description]]",
					"title": "Ingresos",
					"fillAlphas": 0,
					"valueField": "ingresos",
					"valueAxis": "ingresosAxis"
				}
			],
			"chartCursor": {
				"cursorAlpha": 0.2,
				"cursorColor":"#5b0218",
				"fullWidth":true,
				"valueBalloonsEnabled": false,
				"zoomable": false
			},
			"categoryField": "mes",
			"categoryAxis": {
				"autoGridCount": false,
				"axisColor": "#5b0218",
				"gridAlpha": 0.1,
				"gridColor": "#5b0218",
				"gridCount": 50,
				"labelRotation": 30
			},
			"export": {
				"enabled": false
			}
		});
	}
	
	graficaTiemposEntrega = function(){
		var graficaTiemposEntrega = AmCharts.makeChart( "graficaTiemposEntrega", {
			"type": "radar",
			"theme": "none",
			"dataProvider": dataTiemposEntrega,
			"startDuration": 1,
			"valueAxes": [{
				//"gridType": "circles",
				"minimum": 0
			}],
			"graphs": [ {
				"balloonText": "[[value]] Ordenes en el mes \n[[descripcion]]",
				"bullet": "round",
				"lineColor": "#5b0218",
				"lineThickness": 2,
				"valueField": "entregas"
			} ],
			"categoryField": "etiqueta",
			"export": {
				"enabled": true
			}
		});
	}
	
	$scope.tabMasV = true;
	$scope.tabMenosV = false;
	$scope.tabSucVen = true;
	$scope.tabSucIng = false;
	
	$scope.tabsTOP10 = [
   		{idTab: 1, nbTab: 'Más Vendidos', isActive: true},
   		{idTab: 2, nbTab: 'Menos Vendidos', isActive: false}
   	];

	$scope.tabsProv = [
   		{idTab: 1, nbTab: 'Tiempos Entrega', isActive: true},
   		{idTab: 2, nbTab: 'Cambios de Precio', isActive: false}
   	];
	
	$scope.changeGraph = function(tab) {
		switch (tab) {
			case 'Menos Vendidos':
				$scope.tabsTOP10[0].isActive = false;
				$scope.tabsTOP10[1].isActive = true;
				$scope.tabMenosV = true;
				$scope.tabMasV = false;
				break;
			case 'Más Vendidos':
				$scope.tabsTOP10[1].isActive = false;
				$scope.tabsTOP10[0].isActive = true;
				$scope.tabMenosV = false;
				$scope.tabMasV = true;
				break;
			case 'Cambios de Precio':
				$scope.tabsProv[0].isActive = false;
				$scope.tabsProv[1].isActive = true;
				$scope.tabProvCPre = true;
				$scope.tabProvTEnt = false;
//				$timeout(function() {
//					graficaIngGanSucursal();
//				});
				break;
			case 'Tiempos Entrega':
				$scope.tabsProv[1].isActive = false;
				$scope.tabsProv[0].isActive = true;
				$scope.tabProvCPre = false;
				$scope.tabProvTEnt = true;
				$timeout(function() {
					graficaTiemposEntrega();
				});
//				$timeout(function() {
//					graficaMetasXSucursal();
//				});
				break;
			default:
				break;
		}
	}
	
	initController = function() {
		iniciaTop10();
		$timeout(function() {
			graficaVentasXSucursal();
		});
		$timeout(function() {
			$timeout(function() {
				graficaTiemposEntrega();
			});
		});
	}
	
	initController();
});
