angular.module(appTeclo).config(function($routeProvider, $locationProvider) {
	
	$routeProvider.when("/", {
		templateUrl : "login.html",
		controller: "loginController"
	});
	
	$routeProvider.when("/login", {
		templateUrl : "login.html",
		controller: "loginController"
	});
	
	$routeProvider.when("/error", {
		templateUrl : "views/error.html",
	});
	
	$routeProvider.when("/index",{
		templateUrl : "views/index.html",
	});
	
	$routeProvider.when("/accesoNegado", {
		templateUrl : "views/accesoNegado.html",
	});
	
	$routeProvider.otherwise({redirectTo: "/index"});
	
	
	/*___________________________________________________________
    ________________** INICIO -> GESTION SUCURSALES** ________________*/
   	//UBICACION SUCURSALES
	$routeProvider.when("/ubicacionSucursal", {
		templateUrl : "views/sucursales/ubicacionSucursal.html",
		controller : "ubicacionSucursalesController"
	});

   	//SEGUIMIENTO SUCURSALES
	$routeProvider.when("/seguimientoPedido", {
		templateUrl : "views/sucursales/seguimientoPedido.html",
		controller : "seguimientoController"
	});
   	//SEGUIMIENTO SUCURSALES
	$routeProvider.when("/existenciaSucursal", {
		templateUrl : "views/sucursales/existenciaSucursal.html",
		controller : "existenciaSucursalController"
	});	
	/*___________________________________________________________
    ________________** INICIO -> GESTIÓN DE INVENTARIO** ________________*/
    
    // Control de existencia
	$routeProvider.when("/controlExistencia", {
		templateUrl : "views/inventario/controlExistencia.html",
		controller : "controlExistenciaController"
	});
	//CONTROL DE ETRADAS
	$routeProvider.when("/entradasAlmacen", {
		templateUrl : "views/inventario/entradasAlmacen.html",
		controller : "entradasAlmacenController"
	});
	//PRODUCTOS
	$routeProvider.when("/productos", {
		templateUrl : "views/inventario/productos.html",
		controller : "productosController"
	});
	
	/*___________________________________________________________
	_________________** INICIO ->  GESTIÓN DE VENTAS** _______________*/

	// VENTAS
	$routeProvider.when("/carta", {
		templateUrl : "views/ventas/carta.html",
		controller : "cartaController"
	});
	$routeProvider.when("/puntoVenta", {
		templateUrl : "views/ventas/puntoventa.html",
		controller : "puntoVentaController"
	});
	// SEGUIMIENTO DE PEDIDOS
	$routeProvider.when("/pedidos", {
		templateUrl : "views/ventas/seguimientoPedidos.html",
		controller : "seguimientoPedidosController"
	});

	// MONITOREO DE CAJA SUC
	$routeProvider.when("/movimientoParcial", {
		templateUrl : "views/ventasMatriz/movimientoParcial.html",
		controller : "depositoParcialController"
	});		
	
	
	// MONITOREO DE CAJA
	$routeProvider.when("/corte", {
		templateUrl : "views/ventasMatriz/monitoreoCaja.html",
		controller : "monitoreoCajaController"
	});	
	
	$routeProvider.when("/movimiento", {
		templateUrl : "views/ventasMatriz/movimientoCaja.html",
		controller : "movimientoCajaController"
	});	
	$routeProvider.when("/apertura", {
		templateUrl : "views/ventasMatriz/aperturaCaja.html",
		controller : "aperturaCajaController"
	});	
	$routeProvider.when("/corteCaja", {
		templateUrl : "views/ventasMatriz/corteCaja.html",
		controller : "corteCajaController"
	});		

	$routeProvider.when("/listaCorteCaja",{
		templateUrl : "views/ventasMatriz/listaCorteCaja.html",
		controller: "listaCorteCajaController"
	});
	
	
	/*___________________________________________________________
	________** INICIO -> GESTIÓN DE PAGOS ** ________*/
	
	// $routeProvider.when("/registroProveedores", {
	// 	templateUrl : "views/proveedores/registro.html",
	// 	controller: "registroController"
    // });
	
	/*___________________________________________________________
	________** INICIO -> REPORTES Y TABLEROS DE CONTROL ** ________*/
	$routeProvider.when("/tableroControl", {
		templateUrl : "views/dashboard/dashboardVentasSucursal.html",
		controller : "dashboardVentasSucursalController"
	});
	
	$routeProvider.when("/infoPVenta", {
		templateUrl : "views/dashboard/dashboardProveedores.html",
		controller : "dashboardProveedoresController"
	});
	
    /*___________________________________________________________
    ________** INICIO -> ADMINISTRACIÓN CONTROLLERS ** ________*/

	$routeProvider.when("/adminUsuariosModificaClave",{
		templateUrl : "views/administracion/administracionModificaClave.html",
		controller : "administracionModificaClaveController"
	});
	
    //	Configurar Aplicación
	$routeProvider.when("/configuracion", {
		templateUrl : "views/administracion/configuracionApp.html",
		controller: "configuracionAppController"
    });
	
    //	Componentes Web
	$routeProvider.when("/componentesWeb",{
		templateUrl : "views/administracion/resources/pluginsWeb.html",
		controller : "pluginsWebController"
	});
	
});