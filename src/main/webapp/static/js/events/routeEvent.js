angular.module(appTeclo)
.run(function($rootScope,$location,$localStorage,$timeout,
		storageService,loginService,logOutService,$route,jwtService) {
	
	$timeout(function() {		
		logOutService.StartTimer();
	}, true);
	
	$rootScope.$on("$routeChangeStart", function(event, next, current) {

		let titleapp = document.getElementById('titleapp');
		let token = storageService.getToken();

		if ( !token ) {
			$rootScope.$evalAsync(function() {
				//	Destruir el popover al salir de la pantalla de configuracion
				WebuiPopovers.hideAll();
				$location.path('/');
				titleapp.innerHTML = $rootScope.titleapp();
				titleapp.innerText = $rootScope.titleapp();
			});
		}
		else if ( token ) {
			let profile = jwtService.getPerfilUsuario(token);
			if ( profile === 'SUCURSAL' ) { $rootScope.shoppingcar.isSale=true; }
		}
		else if(next.$$route.originalPath === "/" || next.$$route.originalPath === '/index' && token) {
			$location.path('/index');
			titleapp.innerHTML = $rootScope.titleapp();
			titleapp.innerText = $rootScope.titleapp();
		}
		
		else if(next.$$route.originalPath === "/componentesWeb" && token) {
			$location.path('/componentesWeb');
		}
		else if (jwtService.isTokenExpired(token, 0)) {
			loginService.logout();
			logOutService.StopTimer();
			$rootScope.$evalAsync(function() {
				$location.path('/');
			})
		}
		else if (token && !jwtService.hasAccess(next.$$route.originalPath, token)) {
			$location.path('/accesoNegado');
		} else {
			titleapp.innerHTML = `${$rootScope.titleapp()} - ${$rootScope.breadCrumbs.modulo}`;
			titleapp.innerText = `${$rootScope.titleapp()} - ${$rootScope.breadCrumbs.modulo}`;
		}
	});
});