/**
 * @author: César Gómez
 * @version: 1.0.0
 * @name: drag and drop element
 */

// const APP = angular.module(appTeclo);

angular.module(appTeclo).directive('dndElement', ($interval) => {

    return new Object({

        restrict: 'A',
        replace: false,
        scope: {
            classname: "@",
            dragstart: '&',
        },
        link: preLink = (scope,element) => {

            element[0].ondragstart = scope.dragstart;
            element[0].ondragenter = scope.dragenter;

        }
    });

});
