angular.module(appTeclo)
.service('ventaService',
function($http,config) {
	
	const URL = `${config.baseUrl}/dataTmp/venta`;

	this.getPromos = () => { return $http.get(`${URL}/promotion.json`); };

	this.getOrders = () => { return $http.get(`${URL}/orders.json`); };

	this.getStock = () => { return $http.get(`${URL}/mainStock.json`); };

});
