angular.module(appTeclo)
	.service('inventarioService',
		function ($http, config) {

			const URL = `${config.baseUrl}/dataTmp/inventario`;

			this.getTypeSearch = () => { return $http.get(`${URL}/typeSearch.json`); };

			this.getTypeProduct = () => { return $http.get(`${URL}/typeProduct.json`); };

			this.getStock = () => { return $http.get(`${URL}/productStock.json`);};

			this.getPriority = () => { return $http.get(`${URL}/typePriorityRequest.json`);};

			this.getTypeMeasurement = () => { return $http.get(`${URL}/typeMeasurement.json`);};

			this.getProductLot = () => { return $http.get(`${URL}/productLot.json`);};

			this.getOrdersPending = () => { return $http.get(`${URL}/ordersPending.json`);};

		});