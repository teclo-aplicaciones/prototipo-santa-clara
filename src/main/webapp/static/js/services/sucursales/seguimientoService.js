angular.module(appTeclo)
.service('seguimientoService',
function($http,config) {
	
	var app= "/dataTmp/seguimiento";
	var urlConfig = config.baseUrl+app;
	
	this.getTypeSearchSeg = function() {
		return $http.get(urlConfig + "/typeSearchSeg.json"); 
	};

	this.getSucursales = function (){
		return $http.get(urlConfig + "/sucursales.json"); 
	};

	this.getStatus = function() {
		return $http.get(urlConfig +"/typeStatus.json");
	};

	this.getPedidoPorSucursal = function() {
		return $http.get(urlConfig +"/pedidosPorSucursal.json");
	};
	this.getStatusActualiza = function() {
		return $http.get(urlConfig +"/typeStatusActualiza.json");
	};
});