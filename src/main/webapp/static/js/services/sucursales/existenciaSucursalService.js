angular.module(appTeclo)
.service('existenciaSucursalService',
function($http,config) {
	
	var app= "/dataTmp/seguimiento";
	var urlConfig = config.baseUrl+app;
	
	this.getTypeSearchSeg = function() {
		return $http.get(urlConfig + "/typeSearchExistencia.json"); 
	};

	this.getSucursales = function (){
		return $http.get(urlConfig + "/pedidosEstatus.json"); 
	};

	this.getStatus = function() {
		return $http.get(urlConfig +"/typeStatusExistencia.json");
	};
	this.getProductoExistencia = function() {
		return $http.get(urlConfig +"/productoExistencia.json");
	};
});