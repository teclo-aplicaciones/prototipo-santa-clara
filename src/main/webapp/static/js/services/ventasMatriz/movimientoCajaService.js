angular.module(appTeclo)
.service('movimientoCajaService',
function($http,config){

    var app = "/dataTmp/cortecajaMatriz";
    var urlConfig = config.baseUrl+app;

    this.getTypeSearch = function(){
        return $http.get(urlConfig+ "/typeSearchMov.json");
    };
	this.getTypeMovimiento = function() {
		return $http.get(urlConfig +"/typeMovimiento.json");
	};

	this.getDatosMovimiento = function() {
		return $http.get(urlConfig +"/datosMovimiento.json");
	};
	this.getAperturaInicial = function() {
		return $http.get(urlConfig +"/fondoInicial.json");
	};
	this.getDatosTicket = function() {
		return $http.get(urlConfig +"/datosTicket.json");
	};
	this.getDetalleTicket = function() {
		return $http.get(urlConfig +"/detalleTicket.json");
	};
	this.getDatosUser = function() {
		return $http.get(urlConfig +"/datosUser.json");
	};
	this.getAllEmpleados = function() {
		return $http.get(urlConfig +"/allEmpleados.json");
	};	
    this.getTypeSearchDos = function(){
        return $http.get(urlConfig+ "/typeSearchDos.json");
    };

});