angular.module(appTeclo)
.service('aperturaCajaService',
function($http,config){

    var app = "/dataTmp/cortecajaMatriz";
    var urlConfig = config.baseUrl+app;

    this.getTypeSearch = function(){
        return $http.get(urlConfig+ "/typeSearchUser.json");
    };
	this.getDatosUser = function() {
		return $http.get(urlConfig +"/datosUser.json");
	};

	this.getEstatusUser = function() {
		return $http.get(urlConfig +"/typeEstatus.json");
	};
	this.getAperturaInicial = function() {
		return $http.get(urlConfig +"/fondoInicial.json");
	};
});