angular.module(appTeclo)
.service('corteCajaService',
function($http,config){

    var app = "/dataTmp/cortecajaMatriz";
    var urlConfig = config.baseUrl+app;

    this.getTypeSearch = function(){
        return $http.get(urlConfig+ "/typeSearchUser.json");
    };
	this.getTypeMovimiento = function() {
		return $http.get(urlConfig +"/typeMovimiento.json");
	};
	this.getDatosUser = function() {
		return $http.get(urlConfig +"/datosUser.json");
	};
	this.getEstatusUser = function() {
		return $http.get(urlConfig +"/typeEstatusCorte.json");
	};
	this.getDatosMovimiento = function() {
		return $http.get(urlConfig +"/datosMovimiento.json");
	};
	this.getAperturaInicial = function() {
		return $http.get(urlConfig +"/fondoInicial.json");
	};
});