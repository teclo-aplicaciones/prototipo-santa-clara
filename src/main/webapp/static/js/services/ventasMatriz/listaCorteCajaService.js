
angular.module(appTeclo).service('listaCorteCajaService',
		function($http,config){
	var app = "/dataTmp/cortecajaMatriz";
	var urlConfig = config.baseUrl +app;
	
	this.getDatosUser = function() {
		return $http.get(urlConfig +"/datosUser.json");
	};
	this.getDatosTicket = function() {
		return $http.get(urlConfig +"/datosTicket.json");
	};
	this.getDetalleTicket = function() {
		return $http.get(urlConfig +"/detalleTicket.json");
	};	
	this.getHistorialCorteCaja = function() {
		return $http.get(urlConfig +"/historialCajaUser.json");
	};		
});