angular.module(appTeclo)
.service('depositoParcialService',
function($http,config){

    var app = "/dataTmp/cortecajaMatriz";
    var urlConfig = config.baseUrl+app;

    this.getTypeSearch = function(){
        return $http.get(urlConfig+ "/typeSearchMov.json");
    };
	this.getTypeMovimiento = function() {
		return $http.get(urlConfig +"/typeMovimiento.json");
	};

	this.getDatosMovimiento = function() {
		return $http.get(urlConfig +"/datosMovimiento.json");
	};
	this.getAperturaInicial = function() {
		return $http.get(urlConfig +"/fondoInicial.json");
	};
});